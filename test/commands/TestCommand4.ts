import { Command } from '../../src/types/Command';

export default class InfoCommand implements Command {
    public info = {
        name: 'test3',
        trigger: [/^t[ae]st4$/, /^k[eu]k$/],
        shortDescription: 'fourth test command',
        description: 'Command for testing.',
    };

    run(): any {
        return 'abc';
    }
}
