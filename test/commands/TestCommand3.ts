import { Command } from '../../src/types/Command';

export default class InfoCommand implements Command {
    public info = {
        name: 'test3',
        trigger: ['test3', 'tust3'],
        shortDescription: 'third test command',
        description: 'Command for testing.',
    };

    run(): any {
        return 'baz';
    }
}
