import CommandService from '../src/services/CommandService';
import { CommandInfo } from '../src/types/CommandInfo';
import assert, { deepStrictEqual } from 'assert';
import MessageService from '../src/services/MessageService';
import path from 'path';
import { Client } from 'discord.js';
import { FakeGuild, FakeMessage, FakeTextChannel } from '../src/Mock';
import { describe, it } from 'mocha';
import { Command } from '../src/types/Command';

const client = new Client();
const guild = new FakeGuild(client);
const channel = new FakeTextChannel(guild);

const author = {
    id: 'bot id',
    username: 'bot username',
    discriminator: '1234',
    bot: true,
};

// describe('Some module', function () {
//     describe('#someMethod()', function () {
//         it("shouldn't exist", function () {
//             return 'yay!';
//         });
//     });
// });

describe('Command service', function () {
    let commandService!: CommandService;
    it('should init', function () {
        commandService = CommandService.build();
        commandService.registeredCommands = [];
    });
    describe('#registerCommand()', function () {
        it('should register command with string trigger', function () {
            commandService.registerCommand(
                class TestCommand1 implements Command {
                    info: CommandInfo = {
                        name: 'test1',
                        trigger: 'test1',
                        shortDescription: 'first test command',
                        description: 'Command for testing.',
                    };

                    run() {
                        return 'foo';
                    }
                },
            );
            deepStrictEqual(commandService.registeredCommands[0].info.name, 'test1', "Name isn't valid");
            deepStrictEqual(commandService.registeredCommands[0].info.trigger, 'test1', "Trigger isn't valid");
        });
        it('should register command with regexp trigger', function () {
            commandService.registerCommand(
                class TestCommand2 implements Command {
                    info: CommandInfo = {
                        name: 'test2',
                        trigger: /a$/,
                        shortDescription: 'second test command',
                        description: 'Command for testing.',
                    };

                    run() {
                        return 'bar';
                    }
                },
            );

            deepStrictEqual(commandService.registeredCommands[1].info.name, 'test2', "Name isn't valid");
            assert(commandService.registeredCommands[1].info.trigger instanceof RegExp, 'Trigger is not regexp');
            deepStrictEqual(commandService.registeredCommands[1].info.trigger.source, 'a$', "Trigger isn't valid");
        });
    });
    describe('#registerCommands()', function () {
        it('should register commands from dir', function () {
            commandService.registerCommands(path.join(__dirname, './commands'));
        });
    });
});

describe('Message service', function () {
    let messageService!: MessageService;
    const commandService: CommandService = CommandService.build();
    const testCommand = class TestCommand implements Command {
        info: CommandInfo = {
            name: 'test',
            trigger: 'test',
            description: 'Test command.',
            shortDescription: 'test command',
        };

        run() {
            return 'def';
        }
    };
    commandService.registeredCommands = [new testCommand()];

    it('should init', function () {
        messageService = MessageService.build();
    });
    describe('#parseMessage()', function () {
        it('should detect commands by string', async function () {
            commandService.registeredCommands[0].info.trigger = 'abc';
            assert(await messageService.parseMessage(new FakeMessage('.abc', channel, author), '.'), "didn't detect");
        });
        it('should detect commands by regexp', async function () {
            commandService.registeredCommands[0].info.trigger = /a$/;
            assert(await messageService.parseMessage(new FakeMessage('lola', channel, author), '.'), "didn't detect");
            assert(
                !(await messageService.parseMessage(new FakeMessage('lolb', channel, author), '.')),
                'detected wrong',
            );
        });
        it('should detect commands by string array', async function () {
            commandService.registeredCommands[0].info.trigger = ['abc', 'def'];
            assert(
                await messageService.parseMessage(new FakeMessage('.abc', channel, author), '.'),
                "didn't detect first",
            );
            assert(
                await messageService.parseMessage(new FakeMessage('.def', channel, author), '.'),
                "didn't detect second",
            );
        });
        it('should detect commands by regexp array', async function () {
            commandService.registeredCommands[0].info.trigger = [/a$/, /b$/];
            assert(
                await messageService.parseMessage(new FakeMessage('lola', channel, author), '.'),
                'didn\t detect first',
            );
            assert(
                await messageService.parseMessage(new FakeMessage('lolb', channel, author), '.'),
                'didn\t detect second',
            );
            assert(
                !(await messageService.parseMessage(new FakeMessage('lolc', channel, author), '.')),
                'detected third (wrong)',
            );
        });

        commandService.registeredCommands[0].info.trigger = 'test';
        it('should detect string args properly', async function () {
            commandService.registeredCommands[0].info.trigger = 'test';
            commandService.registeredCommands[0].info.args = { a: { type: String, required: true } };
            const parsed = await messageService.parseMessage(new FakeMessage('.test foo', channel, author), '.');
            assert(parsed, "didn't detect command");
            deepStrictEqual(parsed.args.a, 'foo', 'wrong argument');
        });
        it('should detect numeric strings (while waits for string) properly', async function () {
            commandService.registeredCommands[0].info.args = { a: { type: String, required: true } };
            const parsed = await messageService.parseMessage(new FakeMessage('.test 1234', channel, author), '.');
            assert(parsed, "didn't detect command");
            deepStrictEqual(parsed.args.a, '1234', 'wrong argument');
        });
        it('should detect complex strings (with spaces, within quotes) properly', async function () {
            commandService.registeredCommands[0].info.args = { a: { type: String, required: true } };
            const parsed = await messageService.parseMessage(new FakeMessage('.test "abc def"', channel, author), '.');
            assert(parsed, "didn't detect command");
            deepStrictEqual(parsed.args.a, 'abc def', 'wrong argument');
        });
        it('should detect normal strings with escaped quote within properly', async function () {
            commandService.registeredCommands[0].info.args = { a: { type: String, required: true } };
            const parsed = await messageService.parseMessage(new FakeMessage('.test lol\\"kek', channel, author), '.');
            assert(parsed, "didn't detect command");
            deepStrictEqual(parsed.args.a, 'lol"kek', 'wrong argument');
        });
        it('should detect integer args properly', async function () {
            commandService.registeredCommands[0].info.args = { a: { type: Number, required: true } };
            const parsed = await messageService.parseMessage(new FakeMessage('.test 1234', channel, author), '.');
            assert(parsed, "didn't detect command");
            deepStrictEqual(parsed.args.a, 1234, 'wrong argument');
        });
        it('should detect float args properly', async function () {
            commandService.registeredCommands[0].info.args = { a: { type: Number, required: true } };
            const parsed = await messageService.parseMessage(new FakeMessage('.test 12.34', channel, author), '.');
            assert(parsed, "didn't detect command");
            deepStrictEqual(parsed.args.a, 12.34, 'wrong argument');
        });
        it('should detect hexadecimal args properly', async function () {
            commandService.registeredCommands[0].info.args = { a: { type: Number, required: true } };
            const parsed = await messageService.parseMessage(new FakeMessage('.test 0x26', channel, author), '.');
            assert(parsed, "didn't detect command");
            deepStrictEqual(parsed.args.a, 38, 'wrong argument');
        });
        it('should detect multiple args with different types properly', async function () {
            commandService.registeredCommands[0].info.args = {
                a: { type: Number, required: true },
                b: { type: String, required: true },
            };
            const parsed = await messageService.parseMessage(
                new FakeMessage('.test 0x12 foobarbaz', channel, author),
                '.',
            );
            assert(parsed, "didn't detect command");
            deepStrictEqual(parsed.args.a, 18, 'wrong first argument');
            deepStrictEqual(parsed.args.b, 'foobarbaz', 'wrong first argument');
        });

        // todo tests with channels, users, members and roles
    });
});
