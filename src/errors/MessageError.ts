import { MessageEmbed } from 'discord.js';
import Icons from '../Icons';
import color from '../Palette';

export class MessageError {
    public body: string;
    public title = 'Произошла ошибка';
    public color = `${color.GRAY}`;
    public icon?: string;
    public footer?: string;

    constructor(params: { title?: string; body: string; color?: string; icon?: string; footer?: string } | string) {
        if (typeof params === 'string') {
            this.body = params;
        } else {
            this.body = params.body;
            if (params.title) this.title = params.title;
            if (params.color) this.color = params.color;
            if (params.icon) this.icon = params.icon;
            if (params.footer) this.footer = params.footer;
        }
    }

    public toMessage(): MessageEmbed {
        const embed = new MessageEmbed()
            .setAuthor(this.title, Icons.CMD)
            .setColor(this.color)
            .setDescription(this.body);
        if (this.footer) embed.setFooter(this.footer);
        return embed;
    }
}
