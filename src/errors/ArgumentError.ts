import { MessageError } from './MessageError';
import Icons from '../Icons';

export default class ArgumentError extends MessageError {
    constructor(
        command: string,
        prefix: string,
        body?: string,
        possibleUsage?: string,
        title = 'Неправильное использование',
    ) {
        super({
            body: `${body ?? 'Неправильное использование команды'}\n\n${
                possibleUsage ? `Возможно, вы имели в виду:\n${prefix}${possibleUsage}\n\n` : ''
            }Напишите \`${prefix}help ${command}\` для получения помощи.`,
            title,
            icon: Icons.CMD,
        });
    }
}
