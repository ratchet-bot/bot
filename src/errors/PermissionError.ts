import { MessageError } from './MessageError';

export default class PermissionError extends MessageError {
    constructor(body: string, title = 'Нет прав') {
        super({ body, title });
    }
}
