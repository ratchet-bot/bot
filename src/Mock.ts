/* eslint-disable */
import * as Discord from 'discord.js';

let count = 0;

export class FakeGuild extends Discord.Guild {
    // @ts-ignore
    constructor(client) {
        super(client, {
            // you don't need all of these but I just put them in to show you all the properties that Discord.js uses
            id: count++,
            name: '',
            icon: null,
            splash: null,
            owner_id: '',
            region: '',
            afk_channel_id: null,
            afk_timeout: 0,
            verification_level: 0,
            default_message_notifications: 0,
            explicit_content_filter: 0,
            roles: [],
            emojis: [],
            features: [],
            mfa_level: 0,
            application_id: null,
            system_channel_id: null,
        });
        this.client.guilds.cache.set(this.id, this);
    }
}

export class FakeTextChannel extends Discord.TextChannel {
    // @ts-ignore
    constructor(guild) {
        super(guild, {
            id: count++,
            type: 0,
        });
        this.client.channels.cache.set(this.id, this);
    }

    // you can modify this for other things like attachments and embeds if you need
    // @ts-ignore
    send(content) {
        // @ts-ignore
        return this.client.actions.MessageCreate.handle({
            id: count++,
            type: 0,
            channel_id: this.id,
            content,
            author: {
                id: 'bot id',
                username: 'bot username',
                discriminator: '1234',
                bot: true,
            },
            pinned: false,
            tts: false,
            nonce: '',
            embeds: [],
            attachments: [],
            timestamp: Date.now(),
            edited_timestamp: null,
            mentions: [],
            mention_roles: [],
            mention_everyone: false,
        });
    }
}

export class FakeMessage extends Discord.Message {
    // @ts-ignore
    constructor(content, channel, author, mentions = []) {
        super(
            channel.client,
            {
                id: count++,
                type: 0,
                channel_id: channel.id,
                content,
                author,
                pinned: false,
                tts: false,
                nonce: '',
                embeds: [],
                attachments: [],
                timestamp: Date.now(),
                edited_timestamp: null,
                mentions,
                mention_roles: [],
                mention_everyone: false,
            },
            channel,
        );
    }
}
