export enum PunishmentTypes {
    KICK = 'kick',
    BAN = 'ban',
    MUTE = 'mute',
    WARN = 'warn',
}
