import { Arguments, Command } from '../types/Command';
import { GuildMember, Message, MessageEmbed } from 'discord.js';
import color from '../Palette';

const commandArgs = {
    user: {
        type: GuildMember,
        required: false,
        title: 'пользователь',
    },
};
export default abstract class BaseReaction implements Command {
    public info = {
        name: '-',
        trigger: [''],
        shortDescription: '-',
        description: '-',
        args: commandArgs,
    };

    public noUser: string | ((message: Message) => string) = 'Оу...';
    public aloneUser: string | ((message: Message) => string) = 'Оу...';

    public image: () => Promise<string> = async () => '';
    public text: (message: Message, user: GuildMember) => Promise<string[]> = async () => [];

    async run(message: Message, args: Arguments<typeof commandArgs>): Promise<any> {
        const user = args.user;
        if (!user) return message.reply(typeof this.noUser === 'function' ? await this.noUser(message) : this.noUser);
        if (args.user.id == message.author.id)
            return message.reply(typeof this.aloneUser === 'function' ? await this.aloneUser(message) : this.aloneUser);

        return new MessageEmbed()
            .setImage(await this.image())
            .setColor(color.EMBED_COLOR)
            .setAuthor(...((await this.text(message, args.user)) as [string, string]));
    }
}
