enum ActionsTypes {
    WEB = 'web',
    COMMANDS = 'commands',
    REACTIONS = 'reactions',
}

export default ActionsTypes;
