export enum CommandStatus {
    ENABLED,
    NEEDS_ACTION,
    DISABLED_HERE,
    DISABLED,
}
