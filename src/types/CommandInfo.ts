import { PermissionString } from 'discord.js';
import CommandPermissions from './CommandPermissions';
import Argument from './Argument';

export type ValueOf<T> = T[keyof T];

type CommandPermission_ = PermissionString | PermissionString[] | ValueOf<typeof CommandPermissions>;
export type CommandPermission = CommandPermission_ | CommandPermission_[];
export type BotPermission = PermissionString | PermissionString[];

export interface CommandInfo {
    name: string;
    immutable?: boolean;
    aliases?: string[];
    trigger: string | RegExp | string[] | RegExp[];
    permission?: CommandPermission;
    botPermission?: BotPermission;
    cooldown?: number;
    shortDescription: string;
    description: string;
    args?: { [key: string]: Argument } | null;
    usage?: string;
    disabled?: boolean;
    listInHelp?: boolean;
    showInHelp?: boolean;
    category?: string | null;
    regExpCategory?: [string, RegExp[]];
    oneMessage?: boolean;
    disableWrite?: boolean;
}
