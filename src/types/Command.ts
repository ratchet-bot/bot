import { CommandInfo } from './CommandInfo';
import { Client, ClientEvents, Guild, GuildChannel, GuildMember, Message } from 'discord.js';
import Argument from './Argument';
import { DBUser } from '../models/DBUser';
import { DBGuild } from '../models/DBGuild';
import { DBMember } from '../models/DBMember';
import { DocumentType } from '@typegoose/typegoose';
import { CommandStatusObject } from '../services/CommandService';

// type FilterFlags<Base, Condition> = {
//     [Key in keyof Base]:
//     Base[Key] extends Condition ? Key : never
// };

export interface DbInfo {
    guild: DocumentType<DBGuild>;
    user: DocumentType<DBUser>;
    member: DocumentType<DBMember>;
}

export type Arguments<T extends { [key: string]: Argument }> = {
    [key in (keyof T)[][number]]: T[key]['type'] extends StringConstructor
        ? string
        : T[key]['type'] extends NumberConstructor
        ? number
        : T[key]['type']['prototype'];
};

export interface Command {
    info: CommandInfo;
    handlers?: {
        [key in keyof ClientEvents]?:
            | ((...args: ClientEvents[key]) => void | Promise<void>)
            | ((...args: ClientEvents[key]) => void | Promise<void>)[];
    };
    run: (message: Message, args: Arguments<any>, command: string, db: DbInfo, send: any) => any; // todo
    beforeUnloaded?: (client: Client) => void | Promise<void>;
    onLoaded?: (client: Client) => void | Promise<void>;
    getStatus?: (
        message: Message | { guild: Guild; member: GuildMember; channel: GuildChannel },
        db: DbInfo,
    ) => CommandStatusObject | false;
}
