/* eslint-disable @typescript-eslint/ban-types */
import { GuildChannel, GuildMember, Role, User } from 'discord.js';

interface Argument {
    required: true | false;
    type: StringConstructor | NumberConstructor | typeof User | typeof GuildMember | typeof GuildChannel | typeof Role;
    default?: string;
    title?: string;
}

export default Argument;
