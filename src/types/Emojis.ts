enum Emojis {
    ONLINE = '<:rthonline:726795983962374186>',
    IDLE = '<:rthidle:726795984440524871>',
    DND = '<:rthdnd:726795984717217792>',
    OFF = '<:rthoff:726795984457170944>',
    CHANNEL = '<:rthch:726795779276275803>',
    VOICE = '<:rthvc:726795916148998184>',
    OWNER = '<:rthowner:740124251553660990>',
    BOT = '<:rthbot:727485806478098433>',
    EMPTY = '<:rchempty:726771774859247666>',
    USER = '<:rthoneuser:740124057198133298>',
    USERS = '<:rthtwousers:740124136889647124>',
    CHAT = '<:rthchat:740123976545599509>',
    NEWS = '<:rthnews:741026283848335363>',
    STORE = '<:rthstore:741026716222488586>',
    CATEGORY = '<:rthcategory:741028083435438160>',
}

export default Emojis;
