import express, { Application } from 'express';
import { v4 } from 'uuid';

export default class ActionService {
    private static instance?: ActionService;
    public actions: { [key: string]: { [key: string]: () => boolean | Promise<boolean> } } = {};

    private constructor(private app: Application) {
        this.configuredWebServer();
    }

    public static build(): ActionService {
        if (ActionService.instance) return ActionService.instance;
        const app = express();
        app.listen(8080);
        return (ActionService.instance = new ActionService(app));
    }

    public configuredWebServer() {
        this.app.get('/action/:id/:name', async (req, res) => {
            res.send(
                'Если вкладка не закрылась автоматически, пройдите на страницу about:config</a>,' +
                    'найдите там <pre>dom.allow_scripts_to_close_windows</pre> и измените на true.' +
                    'Также, вы можете сменить режим действий в боте на реакции' +
                    '<script>' +
                    "let win = window.open(window.location.href, '_self');window.stop();win.close();window.open('','_parent','');window.close();" +
                    '</script>',
            );
            await this.executeAction(req.params.id, req.params.name);
        });
    }

    public registerAction(handlers: { [key: string]: () => boolean | Promise<boolean> }) {
        const id = v4();
        this.actions[id] = handlers;
        return { id, url: `http://localhost:8080/action/${id}/` };
    }

    public async executeAction(id: string, name: string) {
        if (!(id in this.actions) || !(name in this.actions[id])) return false;
        const result = await this.actions[id][name]();
        if (!result) delete this.actions[id];
        return false;
    }
}
