import { Command, DbInfo } from '../types/Command';
import { Client, ClientEvents, Guild, GuildChannel, GuildMember, Message } from 'discord.js';
import path from 'path';
import Utils from '../Utils';
import Bot from '../Bot';
import chokidar from 'chokidar';
import { CommandStatus } from '../types/CommandStatus';
import scanDir = Utils.scanDir;

export interface CommandStatusObject {
    status: CommandStatus;
    info?: string;
}

export default class CommandService {
    private static instance?: CommandService;
    public registeredCommands: Command[] = [];
    public handlers: {
        [key in keyof ClientEvents]?: ((...args: ClientEvents[key]) => void | Promise<void>)[];
    } = {};

    private constructor() {
        /**/
    }

    public static async build(client?: Client): Promise<CommandService> {
        if (CommandService.instance) return CommandService.instance;
        CommandService.instance = new CommandService();
        if (process.env.NODE_ENV === 'development' && !process.argv.includes('--no-swap'))
            await CommandService.instance.subscribeSwap(client as Client);
        return CommandService.instance;
    }

    async subscribeSwap(client: Client) {
        const watcher = chokidar.watch(path.join(__dirname, '../commands/**/*'), {
            ignored: /(^|[\/\\])\../, // ignore dotfiles
            persistent: true,
        });
        // watcher.on('add', (filePath) => this.registerFile(filePath, path.join(__dirname, '../commands'), client)); todo
        watcher.on('change', (filePath) => this.swapCommand(filePath, client));
        console.log('Swap subscribed successfully');
    }

    async registerCommand(command: any, client: Client, category?: [string, RegExp[]]): Promise<void> {
        const commandObject: Command = new command();
        if (this.registeredCommands.find((e) => e.info.name === commandObject.info.name))
            throw new Error('Command already exists');
        if (!commandObject.info.category && category) commandObject.info.regExpCategory = category;

        if (commandObject.handlers) {
            for (const handler in commandObject.handlers) {
                if (commandObject.handlers.hasOwnProperty(handler)) {
                    const handlerFunction = commandObject.handlers[handler as keyof ClientEvents];
                    if (!(handler in this.handlers)) this.handlers[handler as keyof ClientEvents] = [];
                    this.handlers[handler as keyof ClientEvents]?.push(
                        ...((Array.isArray(handlerFunction) ? handlerFunction : [handlerFunction]).map((e) =>
                            e?.bind(commandObject),
                        ) as ((...args: any[]) => void | Promise<void>)[]),
                    );
                }
            }
        }

        if ('onLoaded' in commandObject && commandObject.onLoaded) await commandObject.onLoaded(client);
        this.registeredCommands.push(commandObject);
        console.log(`Registered command "${commandObject.info.name}"`);
    }

    async unregisterCommand(command: string): Promise<Command> {
        const commandObject: Command | undefined = this.registeredCommands.find((c) => c.info.name === command);
        if (!commandObject) throw new Error('No command found');

        this.registeredCommands = this.registeredCommands.filter((c) => c.info.name !== command);
        console.log(`Unregistered command "${commandObject.info.name}"`);
        return commandObject;
    }

    async swapCommand(filePath: string, client: Client): Promise<boolean> {
        console.log(`Swapping ${path.basename(filePath)}`);
        delete require.cache[require.resolve(filePath)];
        const commandObjectClass: { new (): Command } = require(filePath).default;
        if (!commandObjectClass) return false;

        const commandObject = new commandObjectClass();
        let category;
        try {
            const oldCommandObject = await this.unregisterCommand(commandObject.info.name);
            category = oldCommandObject.info.regExpCategory;
        } catch (e) {
            console.log("Command wasn't registered yet");
            category = CommandService.getCategory(path.dirname(filePath));
        }
        await this.registerCommand(commandObjectClass, client, category);
        return true;
    }

    subscribeEvents(client: Client): void {
        for (const handler in this.handlers) {
            if (this.handlers.hasOwnProperty(handler)) {
                client.on(handler as keyof ClientEvents, async (...params) => {
                    for (const innerHandler of this.handlers[handler as keyof ClientEvents] || []) {
                        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                        // @ts-ignore
                        await innerHandler(...params);
                    }
                });
            }
        }
    }

    async registerCommands(client: Client, folderPath?: string): Promise<void> {
        const folder = path.join(...(folderPath ? [folderPath] : [__dirname, '../commands']));
        for await (const file of scanDir(folder)) {
            try {
                await this.registerFile(file, folder, client);
            } catch (e) {
                console.error(`Failed to register command "${path.basename(file)}": \n${e}`);
            }
        }
    }

    async registerFile(file: string, folder: string, client: Client) {
        if (!file.endsWith('.ts') && !file.endsWith('.js')) return console.error(`${file} is not a command file.`);
        const relative = path.relative(folder, file);
        const category = path.dirname(relative);
        const command = await import(file);
        await this.registerCommand(command.default, client, CommandService.getCategory(category));
    }

    static getCategory(category: string): [string, RegExp[]] {
        return category !== '.'
            ? category in Bot.categories
                ? Bot.categories[category]
                : [category, []] || Bot.categories._
            : Bot.categories._;
    }

    resolveCommand(stringCommand: string): Command | undefined {
        return this.registeredCommands.find((c) => {
            if (typeof c.info.trigger === 'string') return stringCommand === c.info.trigger;
            if (c.info.trigger instanceof RegExp) return stringCommand.match(c.info.trigger);
            if (Array.isArray(c.info.trigger)) {
                return (c.info.trigger as (RegExp | string)[]).find((trigger) => {
                    if (typeof trigger === 'string') return stringCommand === trigger;
                    return stringCommand.match(trigger);
                });
            }
        });
    }

    getStatus(
        command: Command,
        message: Message | { guild: Guild; member: GuildMember; channel: GuildChannel },
        db: DbInfo,
        generic = false,
    ): CommandStatusObject {
        const commandSettings = db.guild.commandSettings?.get(generic ? '_' : command.info.name);
        if (commandSettings && !command.info.immutable) {
            if (!generic && !commandSettings?.enabled)
                return { status: CommandStatus.DISABLED, info: 'Команда отключена администратором.' };
            if (
                commandSettings.deniedChannels &&
                commandSettings.deniedChannels.length &&
                commandSettings.deniedChannels.includes(message.channel.id)
            )
                return {
                    status: CommandStatus.DISABLED_HERE,
                    info: 'Данная команда была отключена в этом канале.\n(список запрещенных каналов)',
                };
            if (
                commandSettings.allowedChannels &&
                commandSettings.allowedChannels.length &&
                !commandSettings.allowedChannels.includes(message.channel.id)
            )
                return {
                    status: CommandStatus.DISABLED_HERE,
                    info: 'Данная команда была отключена в этом канале.\n(список разрешенных каналов)',
                };
        }
        return generic
            ? command?.getStatus?.(message, db) || { status: CommandStatus.ENABLED }
            : this.getStatus(command, message, db, true);
    }
}
