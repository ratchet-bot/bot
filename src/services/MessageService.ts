import {
    Guild,
    GuildChannel,
    GuildMember,
    Message,
    MessageEmbed,
    MessageMentions,
    Role,
    TextChannel,
    User,
    VoiceChannel,
} from 'discord.js';
import ArgumentError from '../errors/ArgumentError';
import CommandService from './CommandService';
import { Command, DbInfo } from '../types/Command';
import CommandPermissions from '../types/CommandPermissions';
import Utils from '../Utils';
import { Action } from '../objects/Action';
import DatabaseService from './DatabaseService';
import { BotPermission, CommandPermission } from '../types/CommandInfo';
import PermissionError from '../errors/PermissionError';
import Bot from '../Bot';
import { CommandStatus } from '../types/CommandStatus';
import generateUsage = Utils.generateUsage;
import { MessageError } from '../errors/MessageError';

export default class MessageService {
    private static instance?: MessageService;

    private constructor() {
        /**/
    }

    public static build(): MessageService {
        return MessageService.instance || (MessageService.instance = new MessageService());
    }

    public permissionErrorMessage(perms: BotPermission, bot = false) {
        if (bot && Array.isArray(perms) && perms.length > 1) {
            return `Для того, чтобы выполнить эту команду,\nботу требуются права ${Utils.list(
                perms.map((e) => '`' + e + '`'),
            )}`;
        } else
            return `Для того, чтобы выполнить эту команду,\n${bot ? 'боту' : 'Вам'} требуется право ${Utils.list(
                (Array.isArray(perms) ? perms : [perms]).map((e) => '`' + e + '`'),
                'или',
            )}`;
    }

    public checkPermissions(
        permission: CommandPermission,
        botPermission: BotPermission,
        member: GuildMember,
        channel: GuildChannel,
        checkBotPerms = true,
        throwErrors = true,
    ) {
        if (
            checkBotPerms &&
            (Array.isArray(botPermission) ? botPermission.length : botPermission) &&
            !member.guild.me?.permissionsIn(channel).has(botPermission) &&
            !member.guild.me?.permissions.has(botPermission)
        )
            throw new PermissionError(this.permissionErrorMessage(botPermission, true));
        if (
            permission === CommandPermissions.OWNER
            // Array.isArray(permission)
            // ? permission.includes(CommandPermissions.OWNER)
            // : permission === CommandPermissions.OWNER
        ) {
            if (!Bot.owners.includes(member.user.id))
                if (throwErrors)
                    throw new PermissionError(
                        'Это действие разрешено только пользователям со статусом `BOT_DEVELOPER`',
                    );
                else return false;
        } else {
            if (!Array.isArray(permission)) permission = [permission];
            for (const right of permission) {
                if (typeof right !== 'string') continue;
                if (!member.permissionsIn(channel).any(right) && !member.permissions.any(right)) {
                    if (throwErrors) throw new PermissionError(this.permissionErrorMessage(right));
                    else return false;
                }
            }
        }
        return true;
    }

    public async parseMessage(
        message: Message,
        prefix: string,
        db: DbInfo,
    ): Promise<
        | {
              command: string;
              args: { [key: string]: any };
              commandObject: Command;
          }
        | undefined
    > {
        let possibleUsage = '';
        if (!message.member || !message.guild || !message.content.trim().startsWith(prefix)) return;
        let args = message.content
            .trim()
            .substring(prefix.length)
            .trim()
            .match(/"(?:\\"|\\\\|[^"])*"|\S+/gs);
        if (!args) return;
        const commandString = args.shift()?.trim();
        if (!commandString) return;
        const commandService = await CommandService.build();
        const commandObject = commandService.resolveCommand(commandString);
        if (!commandObject) return; // no command

        let commandArgs: any[] = [];

        this.checkPermissions(
            commandObject.info.permission || [],
            commandObject.info.botPermission || [],
            message.member,
            message.channel as GuildChannel,
        );

        const status = await commandService.getStatus(commandObject, message, db);
        if (status.status === CommandStatus.DISABLED || status.status === CommandStatus.DISABLED_HERE) return;
        if (status.status === CommandStatus.NEEDS_ACTION && status.info) throw new MessageError(status.info);

        if (commandObject.info.args !== null) {
            args = args.map(
                (arg) =>
                    (arg || '')
                        .replace(/^"(.*)"$/gs, '$1') // remove unwanted quotes ("abc" -> abc)
                        .replace(/(?<!(?<!\\)\\)\\"/gs, '"') // remove backslash before quotes (\" -> ")
                        .replace(/\\\\/gs, '\\'), // replace escaped quotes to normal (\\ -> \)
            );
            commandArgs = Object.keys(commandObject.info.args || []).map((k) => ({
                name: k,
                ...((commandObject.info.args as any)[k] || {}),
            }));

            if (args.length > commandArgs.length && commandArgs.length && commandArgs.slice(-1)[0].type === String) {
                possibleUsage = generateUsage(commandString, [
                    ...args.slice(0, args.length - 2),
                    ...[args.slice(args.length - 2).join(' ')],
                ]);
            }

            if (
                args.length > commandArgs.length || // all arguments
                args.length < commandArgs.filter((e) => !('required' in e) || e.required).length // only required
            )
                throw new ArgumentError(commandString, prefix, 'Неправильное количество аргументов', possibleUsage);
        } else {
            args = [];
        }

        const newArguments: { [key: string]: any } = {};

        // todo simplify this code
        for (const argumentIndex in commandArgs) {
            const neededArgument = commandArgs[argumentIndex];
            const realArgument = args[argumentIndex];
            if (!realArgument && (!('required' in neededArgument) || neededArgument.required))
                throw new ArgumentError(commandString, prefix, `Неправильный аргумент \`${neededArgument.name}\``);
            if (!realArgument) continue;
            switch (neededArgument.type) {
                case String:
                    newArguments[neededArgument.name] = realArgument;
                    break;
                case Number:
                    if (isNaN(Number(realArgument)))
                        throw new ArgumentError(
                            commandString,
                            prefix,
                            `Аргумент \`${neededArgument.name}\` должен быть числом.`,
                        );
                    newArguments[neededArgument.name] = Number(realArgument);
                    break;
                case User:
                    const userMatch = realArgument.match(new RegExp(MessageMentions.USERS_PATTERN.source));
                    if (userMatch) {
                        if (message.mentions.users.has(userMatch[1]))
                            newArguments[neededArgument.name] = message.mentions.users.get(userMatch[1]);
                        else if (message.mentions?.members?.has(userMatch[1]))
                            newArguments[neededArgument.name] = message.mentions.members?.get(userMatch[1])?.user;
                        else newArguments[neededArgument.name] = await message.client.users.fetch(userMatch[1]);
                        if (!newArguments[neededArgument.name])
                            throw new ArgumentError(
                                commandString,
                                prefix,
                                `Пользователь с аргумента \`${neededArgument.name}\` не найден`,
                            );
                    } else {
                        const user =
                            message.client.users.cache.find(
                                (u) =>
                                    u.id === realArgument ||
                                    u.username.trim().toLowerCase() == realArgument.trim().toLowerCase(),
                            ) ||
                            message.guild?.members.cache.find(
                                (m) =>
                                    m.id === realArgument ||
                                    m.nickname?.trim().toLowerCase() === realArgument.trim().toLowerCase() ||
                                    m.user.username.trim().toLowerCase() === realArgument.trim().toLowerCase(),
                            )?.user ||
                            (await message.client.users.fetch(realArgument));
                        if (!user)
                            throw new ArgumentError(
                                commandString,
                                prefix,
                                `Аргумент \`${neededArgument.name}\` должен быть пользователем`,
                            );
                        newArguments[neededArgument.name] = user;
                    }
                    break;
                case GuildMember:
                    const memberMatch = realArgument.match(new RegExp(MessageMentions.USERS_PATTERN.source));
                    if (memberMatch) {
                        if (message.mentions?.members?.has(memberMatch[1]))
                            newArguments[neededArgument.name] = message.mentions.members?.get(memberMatch[1]);
                        else newArguments[neededArgument.name] = message?.guild?.members.cache.get(memberMatch[1]);
                        if (!newArguments[neededArgument.name])
                            throw new ArgumentError(
                                commandString,
                                prefix,
                                `Пользователь с аргумента \`${neededArgument.name}\` не найден на данном сервере`,
                            );
                    } else {
                        const member = message.guild?.members.cache.find(
                            (m) =>
                                m.id === realArgument ||
                                m.nickname?.trim().toLowerCase() === realArgument.trim().toLowerCase() ||
                                m.user.username.trim().toLowerCase() === realArgument.trim().toLowerCase(),
                        );
                        if (!member)
                            throw new ArgumentError(
                                commandString,
                                prefix,
                                `Аргумент \`${neededArgument.name}\` должен быть пользователем данного сервера`,
                            );
                        newArguments[neededArgument.name] = member;
                    }
                    break;
                case Role:
                    const roleMatch = realArgument.match(new RegExp(MessageMentions.ROLES_PATTERN.source));
                    if (roleMatch) {
                        if (message.mentions.roles.has(roleMatch[1]))
                            newArguments[neededArgument.name] = message.mentions.roles.get(roleMatch[1]);
                        else newArguments[neededArgument.name] = message.guild?.roles.cache.get(roleMatch[1]);
                        if (!newArguments[neededArgument.name])
                            throw new ArgumentError(
                                commandString,
                                prefix,
                                `Роль с аргумента \`${neededArgument.name}\` не найдена`,
                            );
                    } else {
                        const role = message.guild?.roles.cache.find(
                            (m) =>
                                m.id === realArgument ||
                                m.name.trim().toLowerCase() === realArgument.trim().toLowerCase(),
                        );
                        if (!role)
                            throw new ArgumentError(
                                commandString,
                                prefix,
                                `Аргумент \`${neededArgument.name}\` должен быть ролью`,
                            );
                        newArguments[neededArgument.name] = role;
                    }
                    break;
                case GuildChannel:
                    const channelMatch = realArgument.match(new RegExp(MessageMentions.CHANNELS_PATTERN.source));
                    if (channelMatch) {
                        if (message.mentions.channels.has(channelMatch[1]))
                            newArguments[neededArgument.name] = message.mentions.channels.get(channelMatch[1]);
                        else newArguments[neededArgument.name] = message.guild?.channels.cache.get(channelMatch[1]);
                        if (!newArguments[neededArgument.name])
                            throw new ArgumentError(
                                commandString,
                                prefix,
                                `Канал с аргумента \`${neededArgument.name}\` не найден`,
                            );
                    } else {
                        const channel = message.guild?.channels.cache.find(
                            (m) =>
                                m.id === realArgument ||
                                m.name.trim().toLowerCase() === realArgument.trim().toLowerCase(),
                        );
                        if (!channel)
                            throw new ArgumentError(
                                commandString,
                                prefix,
                                `Аргумент \`${neededArgument.name}\` должен быть каналом`,
                            );
                        newArguments[neededArgument.name] = channel;
                    }
                    break;
                case VoiceChannel:
                    const voiceMatch = realArgument.match(new RegExp(MessageMentions.CHANNELS_PATTERN.source));
                    if (voiceMatch) {
                        if (message.mentions.channels.has(voiceMatch[1]))
                            newArguments[neededArgument.name] = message.mentions.channels.get(voiceMatch[1]);
                        else newArguments[neededArgument.name] = message.guild?.channels.cache.get(voiceMatch[1]);
                        if (!newArguments[neededArgument.name] || newArguments[neededArgument.name].type !== 'voice')
                            throw new ArgumentError(
                                commandString,
                                prefix,
                                `Войс канал с аргумента \`${neededArgument.name}\` не найден`,
                            );
                    } else {
                        const channel = message.guild?.channels.cache.find(
                            (m) =>
                                m.id === realArgument ||
                                m.name.trim().toLowerCase() === realArgument.trim().toLowerCase(),
                        );
                        if (!channel || channel.type !== 'voice')
                            throw new ArgumentError(
                                commandString,
                                prefix,
                                `Аргумент \`${neededArgument.name}\` должен быть войс каналом`,
                            );
                        newArguments[neededArgument.name] = channel;
                    }
                    break;
                case TextChannel:
                    const textMatch = realArgument.match(new RegExp(MessageMentions.CHANNELS_PATTERN.source));
                    if (textMatch) {
                        if (message.mentions.channels.has(textMatch[1]))
                            newArguments[neededArgument.name] = message.mentions.channels.get(textMatch[1]);
                        else newArguments[neededArgument.name] = message.guild?.channels.cache.get(textMatch[1]);
                        if (!newArguments[neededArgument.name] || newArguments[neededArgument.name].type !== 'voice')
                            throw new ArgumentError(
                                commandString,
                                prefix,
                                `Текст канал с аргумента \`${neededArgument.name}\` не найден`,
                            );
                    } else {
                        const channel = message.guild?.channels.cache.find(
                            (m) =>
                                m.id === realArgument ||
                                m.name.trim().toLowerCase() === realArgument.trim().toLowerCase(),
                        );
                        if (!channel || channel.type !== 'text')
                            throw new ArgumentError(
                                commandString,
                                prefix,
                                `Аргумент \`${neededArgument.name}\` должен быть текстовым каналом`,
                            );
                        newArguments[neededArgument.name] = channel;
                    }
            }
        }

        return {
            commandObject,
            command: commandString,
            args: newArguments,
        };
    }

    public static async resolveAction(guild: Guild, action: Action): Promise<void>;
    public static async resolveAction(guild: Guild, action: Action, channel: TextChannel): Promise<Message>;
    public static async resolveAction(guild: Guild, action: Action, channel?: TextChannel): Promise<any> {
        let message;
        if (channel) message = channel.send(action.toMessage());
        const dbService = await DatabaseService.build();
        const dbGuild = await dbService.getDbGuild(guild);

        try {
            await action.user?.send(action.toMessage());
        } catch (e) {}

        if (dbGuild.logChannel) {
            const logChannel = guild.channels.resolve(dbGuild.logChannel);
            if (!logChannel || logChannel.type !== 'text') return message;
            await (logChannel as TextChannel).send(action.toMessage());
        }
        return message;
    }

    private async sendOrEdit(message: Message, content: any, baseMessage?: Message) {
        if (typeof content === 'string' && content === '' && baseMessage) return baseMessage.delete();
        if (baseMessage)
            return baseMessage.edit(
                typeof content === 'string' ? { content, embed: null } : { content: '', embed: content },
            );
        return message.channel.send(content);
    }

    public async resolveResponse(message: Message, response: any, baseMessage?: Message) {
        if (typeof response === 'string' || typeof response === 'number') {
            if (String(response).startsWith('!!!')) return message.channel.send(String(response).substring(3));
            return this.sendOrEdit(message, String(response), baseMessage);
        }
        if (typeof response === 'object') {
            if (response instanceof Action && message.guild)
                await MessageService.resolveAction(message.guild, response, message.channel as TextChannel);
            else if ('toMessage' in response) return this.sendOrEdit(message, response.toMessage(), baseMessage);
            else if ('toEmbed' in response) return this.sendOrEdit(message, response.toEmbed(), baseMessage);
            else if (response instanceof MessageEmbed) return this.sendOrEdit(message, response, baseMessage);
        }
    }

    public async resolveCommandResponse(
        message: Message,
        response: any | Generator<any> | AsyncGenerator<any>,
        baseMessage?: Message,
    ) {
        if (response && response?.next) {
            let done = false;
            let sendValue = undefined;
            while (!done) {
                const data: { value: any; done: boolean } = await response.next(sendValue);
                done = data.done;
                sendValue = await this.resolveResponse(message, data.value, baseMessage);
            }
        } else {
            await this.resolveResponse(message, response, baseMessage);
        }
    }
}
