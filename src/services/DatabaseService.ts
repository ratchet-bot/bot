import { Guild, GuildMember, User } from 'discord.js';
import mongoose, { Connection, connection } from 'mongoose';
import { DocumentType } from '@typegoose/typegoose';
import DBGuildModel, { DBGuild } from '../models/DBGuild';
import DBUserModel, { DBUser } from '../models/DBUser';
import DBMemberModel, { DBMember } from '../models/DBMember';
import { DBMemberWarn } from '../models/member/DBMemberWarn';

export default class DatabaseService {
    private static instance?: DatabaseService;

    private constructor(private connection: Connection) {}

    public static async build(): Promise<DatabaseService> {
        if ('instance' in DatabaseService && DatabaseService.instance) return DatabaseService.instance;

        await mongoose.connect(process.env.MONGO_URL || '', {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        });
        return (DatabaseService.instance = new DatabaseService(connection));
    }

    private async addGuild(id: string): Promise<void> {
        const guild = new DBGuildModel({ id });
        await guild.save();
    }

    async getDbGuild(guild: Guild): Promise<DocumentType<DBGuild>> {
        const guildList = await DBGuildModel.find({ id: guild.id });
        if (!(0 in guildList)) {
            await this.addGuild(guild.id);
            return this.getDbGuild(guild);
        }

        return guildList[0];
    }

    private async addUser(id: string): Promise<void> {
        const user = new DBUserModel({ id });
        await user.save();
    }

    async getDbUser(user: User): Promise<DocumentType<DBUser>> {
        const userList = await DBUserModel.find({ id: user.id });
        if (!(0 in userList)) {
            await this.addUser(user.id);
            return this.getDbUser(user);
        }

        return userList[0];
    }

    private async addMember(guildId: string, userId: string): Promise<void> {
        const member = new DBMemberModel({ guildId, userId });
        await member.save();
    }

    async getDbMember(member: GuildMember): Promise<DocumentType<DBMember>> {
        const memberList = await DBMemberModel.find({ userId: member.user.id, guildId: member.guild.id });
        if (!(0 in memberList)) {
            await this.addMember(member.guild.id, member.user.id);
            return this.getDbMember(member);
        }

        return memberList[0];
    }

    async getDbMembersToUnmute(time: number): Promise<DocumentType<DBMember>[]> {
        return DBMemberModel.find({ muted: { $lte: time, $gt: 0 } });
    }

    async getWarn(id: number): Promise<{ warn: DBMemberWarn; member: DocumentType<DBMember> } | undefined> {
        const member = await DBMemberModel.findOne().where('warns').elemMatch({ id });
        if (!member) return;
        const warn = member.warns.find((w) => w.id === id);
        if (!warn) return;
        return { warn, member };
    }

    async getGamePlayers(game: string): Promise<DocumentType<DBUser>[]> {
        return DBUserModel.find({ ['gameScores.' + game]: { $exists: true } });
    }
}
