import { GuildChannel, MessageEmbed, User } from 'discord.js';
import Icons from '../Icons';
import Utils from '../Utils';
import formatDuration = Utils.formatDuration;
import TextEscape = Utils.TextEscape;
import EmojisEscape = Utils.EmojisEscape;

export class Action {
    public title: string;
    public icon: string;
    public color = '#585f63';
    public user?: User;
    public moderator?: User;
    public reason?: string;
    public time?: number;
    public channel?: GuildChannel;
    public id?: number;

    constructor(params: {
        id?: number;
        title: string;
        icon: Icons;
        color?: string;
        user?: User;
        moderator?: User;
        reason?: string;
        time?: number;
        channel?: GuildChannel;
    }) {
        this.title = params.title;
        this.icon = params.icon;
        if (params.id) this.id = params.id;
        if (params.color) this.color = params.color;
        if (params.user) this.user = params.user;
        if (params.moderator) this.moderator = params.moderator;
        if (params.reason) this.reason = params.reason;
        if (params.time) this.time = params.time;
        if (params.channel) this.channel = params.channel;
    }

    public toMessage(): MessageEmbed {
        const e = (text: string) =>
            Utils.escape(text, true, EmojisEscape.KEEP_EMOJIS, TextEscape.REMOVE, EmojisEscape.REMOVE_EMOJIS);
        const embed = new MessageEmbed().setAuthor(this.title, this.icon).setColor(this.color);
        if (this.id) embed.addField('ID', this.id, true);
        if (this.user) embed.addField('Пользователь', `${this.user}ㆍ\`${e(this.user.tag)}\``, true);
        if (this.moderator) embed.addField('Администратор', `${this.moderator}ㆍ\`${e(this.moderator.tag)}\``, true);
        if (this.channel) embed.addField('Канал', `${this.channel}ㆍ\`#${e(this.channel.name)}\``, true);
        if (this.reason)
            embed.addField(
                'Причина',
                Utils.escape(this.reason, false, EmojisEscape.KEEP_EMOJIS, TextEscape.KEEP, EmojisEscape.REMOVE_EMOJIS),
                false,
            );
        if (this.time) embed.addField('Время', formatDuration(this.time), true);
        return embed;
    }
}
