import ActionService from '../services/ActionService';
import { DMChannel, GuildMember, Message, MessageEmbed, TextChannel } from 'discord.js';
import ActionsTypes from '../types/ActionsTypes';

interface ModalProps {
    color?: string;
    title?: string;
    body?: string;
    type: ActionsTypes;
    member?: GuildMember;
    buttons: { title: string; name: string; icon: string; action: () => boolean | Promise<boolean> }[];
}

export default class Modal {
    private action: { id: string; url: string };
    public color?: string;
    public title?: string;
    public body?: string;
    public buttons: { title: string; name: string; icon: string; action: () => boolean | Promise<boolean> }[];
    private constructor(props: ModalProps) {
        if (props.color) this.color = props.color;
        if (props.title) this.title = props.title;
        if (props.body) this.body = props.body;

        this.buttons = props.buttons;
        const actionService = ActionService.build();
        this.action = actionService.registerAction(
            Object.fromEntries(new Map(props.buttons.map((e) => [e.name, e.action]))),
        );
    }

    toEmbed(): MessageEmbed {
        const embed = new MessageEmbed();
        if (this.color) embed.setColor(this.color);
        if (this.title) embed.setTitle(this.title);
        if (this.body) embed.setDescription(this.body);
        return embed;
    }

    static async send(channel: TextChannel | DMChannel, props: ModalProps, text = ''): Promise<Message> {
        return new Promise(async (resolve) => {
            const modal = new Modal(props);
            const embed = modal.toEmbed();
            const reactions: { [key: string]: () => void } = {};
            const actionService = ActionService.build();

            switch (props.type) {
                case ActionsTypes.WEB:
                    for (const obj of modal.buttons) {
                        const frame = props.member?.presence.clientStatus?.mobile ? '' : '```';
                        embed.addField(
                            '\u200b',
                            `[${frame}${obj.icon ? obj.icon + ' ' : ''}${obj.title}${frame}](${modal.action.url}${
                                obj.name
                            })`,
                            true,
                        );
                    }
                    break;
                case ActionsTypes.REACTIONS:
                    for (const obj of modal.buttons) {
                        reactions[obj.icon] = () => actionService.executeAction(modal.action.id, obj.name);
                    }
            }

            const msg = await channel.send(text, embed);

            for (const reaction in reactions) {
                await msg.react(reaction);
            }

            if (props.type === ActionsTypes.REACTIONS) {
                const collector = msg.createReactionCollector(
                    (r, u) => Object.keys(reactions).includes(r.emoji.name) && !u.bot,
                    {
                        time: 60000,
                    },
                );
                collector.on('collect', (r) => reactions[r.emoji.name]());
                collector.on('end', (c) => {
                    if (!c.size && modal.buttons.map((b) => b.name).includes('cancel'))
                        actionService.executeAction(modal.action.id, 'cancel');
                });
            }

            resolve(msg);
        });
    }

    static async modalAsync(
        channel: TextChannel | DMChannel,
        props: Omit<ModalProps, 'buttons'>,
        text = '',
        cancel?: () => boolean | Promise<boolean>,
    ): Promise<Message> {
        return new Promise(async (resolve, reject) => {
            const msg = await Modal.send(
                channel,
                {
                    ...props,
                    buttons: [
                        {
                            title: 'Да',
                            name: 'confirm',
                            icon: '✅',
                            action() {
                                resolve(msg);
                                return false;
                            },
                        },
                        {
                            title: 'Нет',
                            name: 'cancel',
                            icon: '❌',
                            async action() {
                                const fn =
                                    cancel ||
                                    (async () => {
                                        await msg.delete();
                                        return false;
                                    });
                                const res = await fn();
                                if (!res) reject('canceled');
                                return res;
                            },
                        },
                    ],
                },
                text,
            );
        });
    }
}
