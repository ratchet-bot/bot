import { DocumentType, getModelForClass, modelOptions, prop } from '@typegoose/typegoose';
import { SwearStats } from './SwearStats';
import { DBUserSettings } from './user/DBUserSettings';
import moment from 'moment';

@modelOptions({ schemaOptions: { collection: 'users' } })
export class DBUser {
    @prop({ required: true, unique: true, index: true })
    public id!: string;

    @prop({ set: (val) => val, get: (val?: boolean) => val ?? false })
    public blacklisted!: boolean;

    @prop({ set: (val) => val, get: (val?: boolean) => val ?? true })
    public premium!: boolean;

    @prop()
    public prefix?: string;

    @prop()
    public description?: string;

    @prop({ default: {} })
    public settings!: DBUserSettings;

    @prop({ required: true, default: 0 })
    public xp!: number;

    @prop()
    public swear?: SwearStats;

    @prop({ type: Number, default: {} })
    public gameScores!: Map<string, number>;

    public async addXP(this: DocumentType<DBUser>, xp: number) {
        this.xp += xp;
        return this.save();
    }

    public async message(this: DocumentType<DBUser>) {
        if (!this.settings?.swearStats?.enabled) return;
        if (!this.swear) {
            this.swear = {
                swear: [],
                messages: 0,
                lastStat: moment().unix(),
            };
        }
        this.swear.messages++;
        return this.save();
    }

    public async swearMessage(this: DocumentType<DBUser>, words: string[]) {
        if (!this.settings?.swearStats?.enabled) return;
        if (!this.swear) {
            this.swear = {
                swear: [],
                messages: 0,
                lastStat: moment().unix(),
            };
        }
        this.swear.swear.push(words);
        this.markModified('swear.swear');
        return this.save();
    }
}

const DBUserModel = getModelForClass(DBUser);
export default DBUserModel;
