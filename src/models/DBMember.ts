import { DocumentType, getModelForClass, modelOptions, prop } from '@typegoose/typegoose';
import { SwearStats } from './SwearStats';
import { DBMemberWarn } from './member/DBMemberWarn';
import moment from 'moment';
import { DBGuild } from './DBGuild';

@modelOptions({ options: { customName: 'members' }, schemaOptions: { collection: 'members' } })
export class DBMember {
    @prop({ required: true })
    public guildId!: string;

    @prop({ required: true })
    public userId!: string;

    /**
     * @description 0 = not muted; -1 = muted forever; anything else = time mute to;
     */
    @prop()
    public muted?: number;

    @prop({ default: 0 })
    public xp!: number;

    @prop()
    public swear?: SwearStats;

    @prop({ required: true, type: DBMemberWarn, default: [] })
    public warns!: DBMemberWarn[];

    public async addXP(this: DocumentType<DBMember>, xp: number) {
        this.xp += xp;
        return this.save();
    }

    public async warn(this: DocumentType<DBMember>, guild: DocumentType<DBGuild>, moderator: string, reason: string) {
        guild.warnsIncrement++;
        await guild.save();
        const warn = { id: guild.warnsIncrement, moderator, reason, time: moment().unix() };
        this.warns = [...this.warns, warn];
        await this.save();
        return warn;
    }

    public async unWarn(this: DocumentType<DBMember>, id: number) {
        this.warns = this.warns.filter((w) => w.id !== id);
        return this.save();
    }

    /**
     * @param time 0 = not muted; -1 = muted forever; anything else = time mute to
     */
    public async mute(this: DocumentType<DBMember>, time: number) {
        this.muted = time;
        return this.save();
    }

    public async message(this: DocumentType<DBMember>, guild: DocumentType<DBGuild>) {
        if (!guild.settings?.swearStats?.enabled) return;
        if (!this.swear) {
            this.swear = {
                swear: [],
                messages: 0,
                lastStat: moment().unix(),
            };
        }
        this.swear.messages++;
        return this.save();
    }

    public async swearMessage(this: DocumentType<DBMember>, guild: DocumentType<DBGuild>, words: string[]) {
        if (!guild.settings?.swearStats?.enabled) return;
        if (!this.swear) {
            this.swear = {
                swear: [],
                messages: 0,
                lastStat: moment().unix(),
            };
        }
        this.swear.swear.push(words);
        this.markModified('swear.swear');
        return this.save();
    }
}

const DBMemberModel = getModelForClass(DBMember);
export default DBMemberModel;
