import { modelOptions, prop } from '@typegoose/typegoose';

@modelOptions({ schemaOptions: { _id: false } })
export class DBMemberWarn {
    @prop({ required: true })
    public id!: number;

    @prop()
    public reason?: string;

    @prop({ required: true })
    public moderator!: string;

    @prop({ required: true })
    public time!: number;
}
