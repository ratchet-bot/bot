import { modelOptions, prop } from '@typegoose/typegoose';
import { DBGuildSettingsBaseLog } from './DBGuildSettingsBase';
import { Punishment } from '../Punishment';

@modelOptions({ schemaOptions: { _id: false } })
export class DBGuildSettingsWarn extends DBGuildSettingsBaseLog {
    @prop()
    public limit?: number;

    @prop({ set: (val) => val, get: (val?: boolean) => val ?? false })
    public punishment?: Punishment | false;
}
