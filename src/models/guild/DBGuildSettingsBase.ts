import { modelOptions, prop } from '@typegoose/typegoose';

@modelOptions({ schemaOptions: { _id: false } })
export class DBGuildSettingsBase {
    @prop({ required: true, default: true })
    public enabled!: boolean;
}

@modelOptions({ schemaOptions: { _id: false } })
export class DBGuildSettingsBaseLog extends DBGuildSettingsBase {
    @prop({ set: (val) => val, get: (val?: boolean) => val ?? true })
    public log?: boolean;

    @prop()
    public logChannel?: string;
}
