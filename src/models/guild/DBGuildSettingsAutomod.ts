import { modelOptions, prop } from '@typegoose/typegoose';
import { DBGuildSettingsBaseLog } from './DBGuildSettingsBase';
import { Punishment } from '../Punishment';

@modelOptions({ schemaOptions: { _id: false } })
export class DBGuildSettingsAutomod extends DBGuildSettingsBaseLog {
    @prop({ set: (val) => val, get: (val?: boolean) => val ?? false })
    public invites?: Punishment | false;

    @prop({ set: (val) => val, get: (val?: boolean) => val ?? false })
    public links?: Punishment | false;

    @prop({ set: (val) => val, get: (val?: boolean) => val ?? false })
    static swear?: Punishment | false;
}
