import { modelOptions, prop } from '@typegoose/typegoose';

@modelOptions({ schemaOptions: { _id: false } })
export class DBGuildCommandSettings {
    @prop({ required: true, default: true })
    public enabled!: boolean;

    @prop({ type: String })
    public allowedChannels?: string[];

    @prop({ type: String })
    public deniedChannels?: string[];
}
