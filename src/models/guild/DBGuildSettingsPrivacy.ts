import { modelOptions, prop } from '@typegoose/typegoose';
import { DBGuildSettingsBase } from './DBGuildSettingsBase';

@modelOptions({ schemaOptions: { _id: false } })
export class DBGuildSettingsPrivacy extends DBGuildSettingsBase {
    @prop({ set: (val) => val, get: (val?: boolean) => val ?? false })
    public listServer?: boolean;

    @prop({ set: (val) => val, get: (val?: boolean) => val ?? true })
    public showInOwnersProfile?: boolean;

    @prop({ type: Boolean })
    public serverInfo?: Map<string, boolean>;
}
