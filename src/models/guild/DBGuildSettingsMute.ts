import { modelOptions, prop } from '@typegoose/typegoose';
import { DBGuildSettingsBaseLog } from './DBGuildSettingsBase';

@modelOptions({ schemaOptions: { _id: false } })
export class DBGuildSettingsMute extends DBGuildSettingsBaseLog {
    @prop()
    public roleId?: string;
}
