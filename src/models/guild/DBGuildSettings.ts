import { prop } from '@typegoose/typegoose';
import { DBGuildSettingsMute } from './DBGuildSettingsMute';
import { DBGuildSettingsWarn } from './DBGuildSettingsWarn';
import { DBGuildSettingsAutomod } from './DBGuildSettingsAutomod';
import { DBGuildSettingsRank } from './DBGuildSettingsRank';
import { DBGuildSettingsBase } from './DBGuildSettingsBase';
import { DBGuildSettingsPrivacy } from './DBGuildSettingsPrivacy';

export class DBGuildSettings {
    @prop({ default: {} })
    public mute!: DBGuildSettingsMute;

    @prop({ default: {} })
    public warn!: DBGuildSettingsWarn;

    @prop({ default: {} })
    public automod!: DBGuildSettingsAutomod;

    @prop({ default: {} })
    public rank!: DBGuildSettingsRank;

    @prop({ default: {} })
    public swearStats!: DBGuildSettingsBase;

    @prop({ default: {} })
    public privacy!: DBGuildSettingsPrivacy;
}
