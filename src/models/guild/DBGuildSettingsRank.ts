import { modelOptions, prop } from '@typegoose/typegoose';
import { DBGuildSettingsBase } from './DBGuildSettingsBase';

@modelOptions({ schemaOptions: { _id: false } })
export class DBGuildSettingsRank extends DBGuildSettingsBase {
    @prop({ set: (val) => val, get: (val?: boolean) => val ?? true })
    public notify?: boolean;

    @prop({ type: String })
    public allowedChannels?: string[];

    @prop({ type: String })
    public deniedChannels?: string[];

    @prop()
    public redirectChannel?: string;
}
