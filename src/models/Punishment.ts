import { modelOptions, prop } from '@typegoose/typegoose';
import { PunishmentTypes } from '../types/PunishmentTypes';

@modelOptions({ schemaOptions: { _id: false } })
export class Punishment {
    @prop({ enum: PunishmentTypes, required: true })
    type!: PunishmentTypes;

    @prop()
    days?: number;

    @prop()
    time?: number;
}
