import { modelOptions, mongoose, prop, Severity } from '@typegoose/typegoose';

@modelOptions({ schemaOptions: { _id: false }, options: { allowMixed: Severity.ALLOW } })
export class SwearStats {
    @prop({ required: true, default: 0 })
    public messages!: number;

    @prop({ type: mongoose.Schema.Types.Mixed, required: true, default: [] })
    public swear!: string[][];

    @prop({ required: true })
    public lastStat!: number;
}
