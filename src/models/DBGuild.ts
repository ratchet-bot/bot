import { getModelForClass, modelOptions, prop } from '@typegoose/typegoose';
import { DBGuildSettings } from './guild/DBGuildSettings';
import { DBGuildCommandSettings } from './guild/DBGuildCommandSettings';
import Bot from '../Bot';

@modelOptions({ schemaOptions: { collection: 'guilds' } })
export class DBGuild {
    @prop({ type: String, required: true, unique: true, index: true })
    public id!: string;

    @prop({ set: (val) => val, get: (val?: boolean) => val ?? false })
    public blacklisted?: boolean;

    @prop({ set: (val) => val, get: (val?: string) => val ?? Bot.defaultPrefix })
    public prefix!: string;

    @prop()
    public description?: string;

    @prop({ set: (val) => val, get: (val?: boolean) => val ?? false })
    public logChannel?: string | false;

    @prop({ required: true, default: 0 })
    public warnsIncrement!: number;

    @prop({ default: {} })
    public settings!: DBGuildSettings;

    @prop({ type: DBGuildCommandSettings, default: {} })
    public commandSettings!: Map<string, DBGuildCommandSettings>;
}

const DBGuildModel = getModelForClass(DBGuild);
export default DBGuildModel;
