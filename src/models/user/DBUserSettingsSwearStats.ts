import { modelOptions, prop } from '@typegoose/typegoose';

@modelOptions({ schemaOptions: { _id: false } })
export class DBUserSettingsSwearStats {
    @prop({ required: true, default: true })
    public enabled!: boolean;

    @prop({ required: true, default: 7 })
    interval!: number;
}
