import { modelOptions, prop } from '@typegoose/typegoose';
import ActionsTypes from '../../types/ActionsTypes';

@modelOptions({ schemaOptions: { _id: false } })
export class DBUserSettingsInterface {
    @prop({ enum: ActionsTypes })
    public preference?: ActionsTypes;
}
