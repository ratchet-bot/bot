import { modelOptions, prop } from '@typegoose/typegoose';

@modelOptions({ schemaOptions: { _id: false } })
export class DBUserSettingsRank {
    @prop({ required: true, default: true })
    public enabled!: boolean;

    @prop({ set: (val) => val, get: (val?: boolean) => val ?? true })
    public notify?: boolean;
}
