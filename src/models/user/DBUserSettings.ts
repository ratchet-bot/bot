import { modelOptions, prop } from '@typegoose/typegoose';
import { DBUserSettingsRank } from './DBUserSettingsRank';
import { DBUserSettingsInterface } from './DBUserSettingsInterface';
import { DBUserSettingsSwearStats } from './DBUserSettingsSwearStats';

@modelOptions({ schemaOptions: { _id: false } })
export class DBUserSettings {
    @prop({ default: {} })
    public rank!: DBUserSettingsRank;

    @prop({ default: {} })
    public interface!: DBUserSettingsInterface;

    @prop({ default: {} })
    public swearStats!: DBUserSettingsSwearStats;
}
