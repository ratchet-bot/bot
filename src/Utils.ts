import util from 'util';
import path from 'path';
import fs, { Stats } from 'fs';
import slash from 'slash';
import { Message, MessageEmbed } from 'discord.js';
import { MessageError } from './errors/MessageError';
import humanizeDuration from 'humanize-duration';
import m from 'moment';

namespace Utils {
    export async function* scanDir(
        dirPath: string,
        statFunction?: (res: string, callback: (...args: any[]) => void) => void,
    ): AsyncGenerator<string> {
        const readdir = util.promisify(fs.readdir);
        const stat: (res: string) => Promise<Stats> = util.promisify(statFunction || fs.stat);

        const dirs = await readdir(dirPath);
        for (const dir of dirs) {
            const res = path.resolve(dirPath, dir);
            if ((await stat(res)).isDirectory()) {
                yield* scanDir(res);
            } else {
                yield slash(res);
            }
        }
    }
    export function decForInt(num: number, strings: string[]): string {
        const cases: any = [2, 0, 1, 1, 1, 2];
        return strings[num % 100 > 4 && num % 100 < 20 ? 2 : cases[num % 10 < 5 ? num % 10 : 5]];
    }
    export function randomInt(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    export function calculateLevel(xp: number): { xpLeft: number; level: number } {
        // 5 * (lvl ^ 2) + 50 * lvl + 100
        let needed = 0;
        let level = 0;
        while (xp >= needed) {
            level++;
            needed += neededToNextLevel(level);
        }

        return { xpLeft: needed - xp, level };
    }

    export function neededToNextLevel(level: number) {
        return 5 * level ** 2 + 50 * level + 100;
    }

    export function progress(percent: number, length: number, filledSymbol: string, notFilledSymbol = ' ') {
        return Array(length)
            .fill(0)
            .map((e, i) => (i / (length - 1) <= percent ? filledSymbol : notFilledSymbol))
            .join('');
    }

    export enum EmojisEscape {
        KEEP_EMOJIS,
        ESCAPE_EMOJIS,
        REMOVE_EMOJIS,
    }

    export enum TextEscape {
        ESCAPE_EVERYTHING,
        ESCAPE_BLOCKS,
        IN_CODE_BLOCK,
        REMOVE,
        KEEP,
    }

    export function e(strings: TemplateStringsArray, ...data: any[]) {
        return data.reduce((prev, curr, i) => prev + Utils.escape(curr) + strings[i + 1], strings[0]);
    }

    export function esc(
        removeNewlines?: boolean,
        emojisEscapeMode?: EmojisEscape,
        textEscapeMode?: TextEscape,
        customEmojisEscapeMode?: EmojisEscape,
    ) {
        return (strings: TemplateStringsArray, ...data: any[]) =>
            data.reduce(
                (prev, curr, i) =>
                    prev +
                    Utils.escape(curr, removeNewlines, emojisEscapeMode, textEscapeMode, customEmojisEscapeMode) +
                    strings[i + 1],
                strings[0],
            );
    }

    export function escape(
        text: any,
        removeNewlines = true,
        emojisEscapeMode: EmojisEscape = EmojisEscape.ESCAPE_EMOJIS,
        textEscapeMode: TextEscape = TextEscape.ESCAPE_EVERYTHING,
        customEmojisEscapeMode: EmojisEscape = EmojisEscape.REMOVE_EMOJIS,
    ) {
        if (typeof text !== 'string') return text;
        text = text.replace(/\\/g, '\\\\');
        switch (textEscapeMode) {
            case TextEscape.ESCAPE_EVERYTHING:
                text = text.replace(/([`*_~])/g, '\\$1');
                break;
            case TextEscape.REMOVE:
                text = text.replace(/([`*_~])/g, '');
                break;
            case TextEscape.ESCAPE_BLOCKS:
                text = text.replace(/`/g, '\\`');
                break;
            case TextEscape.IN_CODE_BLOCK:
                text = text.replace(/`/g, '\u200b`\u200b');
                break;
        }

        const emojisRegExp = /(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])/g;

        switch (emojisEscapeMode) {
            case EmojisEscape.ESCAPE_EMOJIS:
                text = text.replace(emojisRegExp, '\\$1');
                break;
            case EmojisEscape.REMOVE_EMOJIS:
                text = text.replace(emojisRegExp, '');
                break;
        }

        switch (customEmojisEscapeMode) {
            case EmojisEscape.ESCAPE_EMOJIS:
                text = text.replace(/(<a?:[a-zA-Z0-9_]+:\d+>)/g, '\\$1');
                break;
            case EmojisEscape.REMOVE_EMOJIS:
                text = text.replace(/(<a?:[a-zA-Z0-9_]+:\d+>)/g, '');
                break;
        }

        if (removeNewlines) {
            text = text.replace(/[\r\n]/g, ' ');
        }

        return text;
    }

    export async function waitForMessage(
        message: Message,
        text?: string | MessageEmbed,
        timeout = 60000,
        remove = false,
        sendFunction: any = message.channel.send,
    ): Promise<string | undefined> {
        let sentMessage;
        if (text) sentMessage = await sendFunction(text);
        const messages = await message.channel.awaitMessages((m) => m.author.id === message.author.id, {
            time: timeout,
            max: 1,
        });
        if (!messages.size) throw new MessageError('Отменено');
        if (!messages.first()?.content) throw new Error();
        if (remove && messages?.first()?.deletable) messages?.first()?.delete();
        if (remove && typeof text === 'string' && text.startsWith('!!!')) sentMessage.delete();
        return messages.first()?.content;
    }

    export function pastDuration(date: any) {
        return humanizeDuration(Date.now() - (date || 0), {
            language: 'ru',
            conjunction: ' и ',
            serialComma: false,
            units: ['y', 'mo', 'w', 'd'],
            maxDecimalPoints: 0,
        });
    }

    export function formatDuration(date: any) {
        return humanizeDuration(date || 0, {
            language: 'ru',
            conjunction: ' и ',
            serialComma: false,
            units: ['y', 'mo', 'w', 'd', 'h', 'm', 's'],
            maxDecimalPoints: 0,
        });
    }

    export function formatDate(date: any) {
        return m(new Date(date || 0)).format('Do MMMM YYYY [в] LTS');
    }

    export function list(elements: string[], conjunction = 'и'): string {
        return elements.reduce((prev, curr, i) => {
            let separator;
            if (i === 0) {
                separator = '';
            } else if (i === elements.length - 1) {
                separator = ' ' + conjunction + ' ';
            } else {
                separator = ', ';
            }
            return prev + separator + curr;
        }, '');
    }

    export function generateUsage(command: string, args: string[]) {
        return command + ' ' + args.map((e) => (e.includes(' ') ? `"${e}"` : e)).join(' ');
    }

    export function isIterable(object: any): boolean {
        return (
            object != null &&
            (typeof object[Symbol.iterator] === 'function' || typeof object[Symbol.asyncIterator] === 'function')
        );
    }

    export function isConstructor(el: any): boolean {
        try {
            Reflect.construct(String, [], el);
        } catch (e) {
            return false;
        }
        return true;
    }

    export function asyncTimeout(timeout: number): Promise<void> {
        return new Promise<void>((resolve) => setTimeout(resolve, timeout));
    }

    export function random<T>(items: T[]): T {
        return items[Math.floor(Math.random() * items.length)];
    }
}

export default Utils;
