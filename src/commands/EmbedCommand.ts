/* eslint-disable @typescript-eslint/no-unused-vars */
// noinspection ES6UnusedImports
import Discord, { Message, MessageEmbed as Embed } from 'discord.js';
import { Command, DbInfo } from '../types/Command';
import CommandPermissions from '../types/CommandPermissions';
import { CommandInfo } from '../types/CommandInfo';
import { inspect } from 'util';
import { MessageError } from '../errors/MessageError';

// const commandArgs = {};
export default class TestCommand implements Command {
    public info: CommandInfo = {
        name: 'embed',
        trigger: 'embed',
        shortDescription: 'Отправить эмбед',
        description:
            'С помощью этой команды вы можете отправить эмбед в чат.\n\n' +
            'Используйте генератор эмбедов на [этом сайте](https://discohook.org/?message=eyJtZXNzYWdlIjp7ImVtYmVkcyI6W3t9XX19).\n' +
            'При указании нескольких эмбедов, будет отправлен только первый.\n' +
            'Текст сообщения игнорируется.',
        args: null,
        botPermission: 'EMBED_LINKS',
        permission: 'EMBED_LINKS',
        usage: '[данные эмбеда]',
    };

    async *run(message: Message, args: any, command: string, db: DbInfo): AsyncGenerator<any> {
        const raw = message.content.replace(new RegExp('^(?:.*?)' + command, 'gi'), '');
        try {
            await message.delete();
        } catch (e) {}
        try {
            const oldData = JSON.parse(raw);
            if ('embeds' in oldData) oldData.embed = new Embed(oldData.embeds[0]);
            else throw new Error();
            await message.channel.send({ embed: oldData.embed });
        } catch (e) {
            const msg = yield 'Не удалось отправить сообщение';
            (msg as Message).delete({ timeout: 3000 }).then(() => undefined);
        }
    }
}
