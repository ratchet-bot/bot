import { Arguments, Command, DbInfo } from '../types/Command';
import { GuildMember, Message, MessageEmbed } from 'discord.js';
import Utils from '../Utils';
import config from '../config';
import Ico from '../Icons';
import color from '../Palette';
import { CommandInfo } from '../types/CommandInfo';
import TextEscape = Utils.TextEscape;
import EmojisEscape = Utils.EmojisEscape;
import DatabaseService from '../services/DatabaseService';

const commandArgs = {
    member: {
        type: GuildMember,
        required: false,
        title: 'пользователь',
    },
};
export default class ProfileCommand implements Command {
    public info: CommandInfo = {
        name: 'profile',
        trigger: [/^проф[еи]ль?$/i, /^profile?$/i],
        aliases: ['профиль', 'profile'],
        shortDescription: 'Профиль пользователя',
        description: 'С помощью этой команды Вы можете просмотреть свою, или чужую статистику.',
        args: commandArgs,
    };

    async run(message: Message, args: Arguments<typeof commandArgs>, command: string, db: DbInfo): Promise<any> {
        const member = args.member || message.member;
        if (member.user.bot) return message.reply(`У бота свои приколы, у него нету профиля **owo**`);
        if (member?.id !== message.author.id) {
            const dbService = await DatabaseService.build();
            db = {
                ...db,
                user: await dbService.getDbUser(member.user),
                member: await dbService.getDbMember(member),
            };
        }

        const { level: levelUser, xpLeft: xpLeftUser } = Utils.calculateLevel(db.user.xp);
        const { level: levelMember, xpLeft: xpLeftMember } = Utils.calculateLevel(db.member.xp);

        const embed = new MessageEmbed()
            .setColor(color.GRAY)
            .setThumbnail(member.user.displayAvatarURL({ size: 4096 }) || 'undefined')
            .setAuthor(`Профиль пользователя ${member.user.tag}`, `${Ico.TWOUSER}`);

        if (db.guild.settings.rank.enabled) {
            embed.addField(
                Utils.e`Уровень на ${message.guild?.name}`,
                `**${levelMember} lv.ㆍ${
                    (
                        ((Utils.neededToNextLevel(levelMember) - xpLeftMember) / Utils.neededToNextLevel(levelMember)) *
                        100
                    ).toFixed(0) + '%'
                }ㆍ${
                    Utils.neededToNextLevel(levelMember) - xpLeftMember
                } XPㆍ${xpLeftMember} XP осталось**\n${Utils.progress(
                    (Utils.neededToNextLevel(levelMember) - xpLeftMember) / Utils.neededToNextLevel(levelMember),
                    15,
                    '▰',
                    '▱',
                )}`,
                false,
            );
        }
        if (levelMember !== levelUser || xpLeftMember !== xpLeftUser)
            embed.addField(
                `Уровень повсеместный`,
                `**${levelUser} lv.ㆍ${
                    (
                        ((Utils.neededToNextLevel(levelUser) - xpLeftUser) / Utils.neededToNextLevel(levelUser)) *
                        100
                    ).toFixed(0) + '%'
                }ㆍ${Utils.neededToNextLevel(levelUser) - xpLeftUser} XPㆍ${xpLeftUser} XP осталось**\n${Utils.progress(
                    (Utils.neededToNextLevel(levelUser) - xpLeftUser) / Utils.neededToNextLevel(levelUser),
                    15,
                    '▰',
                    '▱',
                )}`,
                false,
            );

        if (message.guild?.ownerID == member.id) {
            embed.setAuthor(`Профиль владельца ${member.user.tag}`, `${Ico.OWNER}`);
        }

        if (config.OWNER.includes(member.id)) {
            embed.setAuthor(`Профиль разработчика бота`, `${Ico.USER_RANK_DEV}`);
            embed.setColor(color.BLUE_DEV);
        }

        return embed;
    }
}
