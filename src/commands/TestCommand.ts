import { Arguments, Command, DbInfo } from '../types/Command';
import { Message } from 'discord.js';
import Utils from '../Utils';

const commandArgs = {};
export default class TestCommand implements Command {
    public info = {
        name: 'test',
        trigger: 'test',
        shortDescription: 'test short',
        description: 'test long',
        args: commandArgs,
        showInHelp: false,
    };

    // static async waitForMessage(message: Message, text?: string, timeout = 600000): Promise<string | undefined> {
    //     if (text) await message.channel.send(text);
    //     const messages = await message.channel.awaitMessages((m) => m.author.id === message.author.id, {
    //         time: timeout,
    //         max: 1,
    //     });
    //     if (!messages.size) throw new MessageError('Отменено');
    //     if (!messages.first()?.content) throw new Error();
    //     return messages.first()?.content;
    // }

    //
    //
    async *run(): AsyncGenerator<any> {
        await Utils.asyncTimeout(10000);
        return 'тест';
    }
}
