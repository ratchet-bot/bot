import { Arguments, Command, DbInfo } from '../types/Command';
import { GuildMember, Message, MessageEmbed, version } from 'discord.js';
import config from '../config';
import Utils from '../Utils';
import Ico from '../Icons';
import color from '../Palette';
import CommandService from '../services/CommandService';
import { CommandInfo } from '../types/CommandInfo';
import human from 'humanize-duration';
import { url } from 'inspector';
const commandArgs = {
    //user: {
    //   type: GuildMember,
    //   required: false,
    //   title: 'пользователь',
    // },
};
export default class Botinfo implements Command {
    public info: CommandInfo = {
        name: 'botinfo',
        trigger: [/^(inf[oa])?bot(inf[ao])?$/i, /^(инф[ао]?)?бот(инф[ао]?(рмация)?)?$/i],
        aliases: [`botinfo`, 'бот', 'ботинфо'],
        shortDescription: 'Информация о боте',
        description: 'С помощью этой команды Вы можете посмотреть информацию о боте.',
        args: commandArgs,
    };

    async run(message: Message, args: Arguments<typeof commandArgs>, command: string, db: DbInfo): Promise<any> {
        const commandService = await CommandService.build();
        return message.channel.send(
            new MessageEmbed()
                .setFooter(`Дата создания бота`)
                .setTimestamp(message.client.user?.createdAt)
                .setAuthor(`Информация ${message.client.user?.tag}`, Ico.BOT_GREEN, `https://discord.gg/PpScZ4E`)
                .setColor(color.GREEN)
                .setDescription(
                    `**Репозиторий [GitLab](https://gitlab.com/ratchet-bot)**
**Разработчики: ${config.OWNER.map((u) => '`' + message.client.users.cache.get(u)?.tag + '`').join(', ')}**
**Ссылки на сервера разработчиков: \\[[RatchetBot](https://discord.gg/PpScZ4E) | [NeppedCord](https://discord.gg/skQzaVb)\\]**`,
                )
                .addField(
                    `Версии субъектов`,
                    `**ㆍВерсия бота ${require('../../package.json').version} (${
                        process.env.NODE_ENV === 'development' ? 'Для разработчиков' : 'Стабильная'
                    })
ㆍВерсия discord.js v${version}
ㆍВерсия nodejs ${process.version}**`,
                    true,
                )
                .addField(
                    `Системное`,
                    `**ㆍЗадержка API ${Math.round(message.client.ws.ping)}ms
ㆍИспользуется ${Math.floor(process.memoryUsage().heapUsed / 1e6)}МБ ОЗУ
ㆍКоличество команд ${commandService.registeredCommands.length}
ㆍСвой префикс недоступен
ㆍПрефикс гильдии \`\ ${db.guild.prefix} \`\**`,
                    true,
                ),
        );
    }
}
