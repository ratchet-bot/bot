import { Arguments, Command } from '../types/Command';
import { Message, MessageEmbed } from 'discord.js';
import config from '../config';
import Icons from '../Icons';
import color from '../Palette';
import { CommandInfo } from '../types/CommandInfo';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import Argument from '../types/Argument';
import { MessageError } from '../errors/MessageError';
import Utils from '../Utils';

const commandArgs = {
    from: {
        type: String,
        required: true,
        title: 'с',
    },
    to: {
        type: String,
        required: true,
        title: 'на',
    },
    text: {
        type: String,
        required: true,
        title: 'текст',
    },
};
export default class InviteCommand implements Command {
    public info: CommandInfo = {
        name: 'translate',
        trigger: [/^translat(e|or)?$/i, /^tl$/i, /^перевод(чик)?$/i],
        aliases: ['tl', 'translate', 'перевод'],
        shortDescription: 'Перевести текст',
        description: 'С помощью этой команды Вы сможете перевести какой-либо текст.',
        args: commandArgs,
    };

    async run(message: Message, args: Arguments<typeof commandArgs>): Promise<any> {
        try {
            return new MessageEmbed().setDescription(
                Utils.escape((await require('translatte')(args.text, { to: args.to, from: args.from })).text),
            );
        } catch (e) {
            throw new MessageError('Ошибка перевода');
        }
    }
}
