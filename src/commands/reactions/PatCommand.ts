import superagent from 'superagent';
import BaseReaction from '../../types/ReactionBase';
import { GuildMember, Message } from 'discord.js';
import Icons from '../../Icons';
export default class HugCommand extends BaseReaction {
    public info = {
        name: 'pat',
        trigger: ['pat', 'гладить', 'погладить'],
        shortDescription: 'Погладить пользователя',
        description: 'С помощью данной команды Вы можете погладить кого-либо.',
        args: { user: { type: GuildMember, required: false, title: 'пользователь' } },
    };
    public aloneUser = '*>.<*';
    public noUser = '***-b-***';
    public text = async (message: Message, user: GuildMember) => {
        return [`${message.author.username} гладит ${user.user.username}`, Icons.ACTION_PAT];
    };
    public image = async () => {
        const { body } = await superagent.get(`https://nekos.life/api/v2/img/pat`);
        return body.url;
    };
}
