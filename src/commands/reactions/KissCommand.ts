import superagent from 'superagent';
import BaseReaction from '../../types/ReactionBase';
import { GuildMember, Message } from 'discord.js';
import Icons from '../../Icons';
export default class HugCommand extends BaseReaction {
    public info = {
        name: 'kiss',
        trigger: ['kiss', 'целоватсья', 'поцеловаться'],
        shortDescription: 'Поцеловать пользователя',
        description: 'С помощью данной команды вы можете поцеловать кого-либо.',
        args: { user: { type: GuildMember, required: false, title: 'пользователь' } },
    };
    public aloneUser = '*>.<*';
    public noUser = 'Жалко что у тебя никого нет -b-';
    public text = async (message: Message, user: GuildMember) => {
        return [`${message.author.username} целуется с ${user.user.username}`, Icons.ACTION_KISS];
    };
    public image = async () => {
        const { body } = await superagent.get(`https://nekos.life/api/v2/img/kiss`);
        return body.url;
    };
}
