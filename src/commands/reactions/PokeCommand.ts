import superagent from 'superagent';
import BaseReaction from '../../types/ReactionBase';
import { GuildMember, Message } from 'discord.js';
import Icons from '../../Icons';
export default class HugCommand extends BaseReaction {
    public info = {
        name: 'poke',
        trigger: ['poke', 'трогать', 'потыркать'],
        shortDescription: 'Ткнуть пользователя',
        description: 'С помощью данной команды вы можете ткнуть кого-либо.',
        args: { user: { type: GuildMember, required: false, title: 'пользователь' } },
    };
    public aloneUser = '*>.<*';
    public noUser = '*o-o**';
    public text = async (message: Message, user: GuildMember) => {
        return [`${message.author.username} трогает ${user.user.username}`, Icons.ACTION_POKE];
    };
    public image = async () => {
        const { body } = await superagent.get(`https://nekos.life/api/v2/img/poke`);
        return body.url;
    };
}
