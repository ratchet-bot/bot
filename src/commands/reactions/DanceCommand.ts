import { GuildMember, Message, MessageEmbed } from 'discord.js';
import { Arguments, Command } from '../../types/Command';
import color from '../../Palette';
import Utils from '../../Utils';

const commandArgs = {};

export default class BaseReaction implements Command {
    public info = {
        name: 'dance',
        trigger: ['dance', 'Dance', 'танец', 'танцевать'],
        shortDescription: 'Танцы, танцы! >w<',
        description: 'Команда для танцулек',
        args: null,
    };

    async run(message: Message, args: Arguments<typeof commandArgs>, command: string): Promise<any> {
        const image = Utils.random([
            'https://cdn.discordapp.com/attachments/739887563111792731/742794577181802576/b59.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742794578071126217/cf9f822ffa1b33b5c4c239e58be63cda57ad488e_hq.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742794576686743652/bWH1s2q.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742794577731387412/b1c.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742794579421560993/ddx90d8-3571fa93-c26f-4128-8f30-77f511cabbec.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742794581224980500/e1ac78c68040a80a305d43dae71cccf95b110d7d_00.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742794582047326208/FearfulUnfoldedBlackfly-small.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742794589366386708/t312312enor.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742794588636577943/FNIkS6R.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742794590750244864/te312nor.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742795013897060442/Mikakunin.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742795066149699664/ThankfulFrightenedDuckbillplatypus-size_restricted.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742797677691142205/Bta5.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742797771706204220/2312.gif',
        ]);

        return new MessageEmbed()
            .setImage(image)
            .setColor(color.EMBED_COLOR)
            .setAuthor(`${message.member?.user.username} танцует`);
    }
}
