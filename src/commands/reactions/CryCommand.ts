import { GuildMember, Message, MessageEmbed } from 'discord.js';
import { Arguments, Command } from '../../types/Command';
import color from '../../Palette';
import Utils from '../../Utils';

const commandArgs = {};

export default class BaseReaction implements Command {
    public info = {
        name: 'cry',
        trigger: ['cry', 'Cry', 'плачь', 'плакать'],
        shortDescription: 'Грустняшка TwT',
        description: 'Команда для грусти, команда-реакция',
        args: null,
    };

    async run(message: Message, args: Arguments<typeof commandArgs>, command: string): Promise<any> {
        const image = Utils.random([
            'https://cdn.discordapp.com/attachments/739887563111792731/742810967322394805/2.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742810970413596822/3.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742810969092128891/1.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742810971118239916/5.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742810971373830266/11.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742810972250570863/8.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742810967166943302/6.jpg',
            'https://cdn.discordapp.com/attachments/739887563111792731/742810973232169001/4.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742810968366514256/10.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742811192120180815/7.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742811325754900480/tenor.gif',
        ]);

        return new MessageEmbed()
            .setImage(image)
            .setColor(color.EMBED_COLOR)
            .setAuthor(`${message.member?.user.username} плачет TwT`);
    }
}
