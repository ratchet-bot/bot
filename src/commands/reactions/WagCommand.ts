import { GuildMember, Message, MessageEmbed } from 'discord.js';
import { Arguments, Command } from '../../types/Command';
import color from '../../Palette';
import Utils from '../../Utils';

const commandArgs = {};

export default class BaseReaction implements Command {
    public info = {
        name: 'wag',
        trigger: ['wag', 'Wag', 'вилять', 'вилять-хвостом'],
        shortDescription: 'Повилять хвостиком UwU',
        description: 'Команда для виляния хвостиком, команда-реакция',
        args: null,
    };

    async run(message: Message, args: Arguments<typeof commandArgs>, command: string): Promise<any> {
        const image = Utils.random([
            'https://cdn.discordapp.com/attachments/739887563111792731/742805639738294422/421412412.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742805639927169085/3c1bdad29ac7df84d31456c5d6636a8f524eec59_hq.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742805642460266646/123.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742805644071010450/12312412.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742805643924209855/123412412412421.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742805643710300230/tenor.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742805641143255161/Horo.full.2273642.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742805642527375550/421412.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742805642267328552/1235124521.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742805641667543123/3a03f328331e16f35cde56c81502073c.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742806176458211408/421412421.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742806245286608926/12412412.gif',
        ]);

        return new MessageEmbed()
            .setImage(image)
            .setColor(color.EMBED_COLOR)
            .setAuthor(`${message.member?.user.username} виляет хвостом UwU`);
    }
}
