import superagent from 'superagent';
import BaseReaction from '../../types/ReactionBase';
import { GuildMember, Message } from 'discord.js';
import Icons from '../../Icons';
export default class HugCommand extends BaseReaction {
    public info = {
        name: 'slap',
        trigger: ['slap', 'ударить', 'датьлеща'],
        shortDescription: 'Дать леща пользователю',
        description: 'С помощью данной команды Вы можете дать леща кому-либо.',
        args: { user: { type: GuildMember, required: false, title: 'пользователь' } },
    };
    public aloneUser = '*>.<*';
    public noUser = 'Нет слов адни эмоции';
    public text = async (message: Message, user: GuildMember) => {
        return [`${message.author.username} дал пощечину ${user.user.username}`, Icons.ACTION_SLAP];
    };
    public image = async () => {
        const { body } = await superagent.get(`https://nekos.life/api/v2/img/slap`);
        return body.url;
    };
}
