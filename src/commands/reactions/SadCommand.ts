import { GuildMember, Message, MessageEmbed } from 'discord.js';
import { Arguments, Command } from '../../types/Command';
import color from '../../Palette';
import Utils from '../../Utils';

const commandArgs = {};

export default class BaseReaction implements Command {
    public info = {
        name: 'sad',
        trigger: ['sad', 'Sad', 'грустняшка', 'грустно'],
        shortDescription: 'Грустняшка TwT',
        description: 'Команда для грусти, команда-реакция',
        args: null,
    };

    async run(message: Message, args: Arguments<typeof commandArgs>, command: string): Promise<any> {
        const image = Utils.random([
            'https://cdn.discordapp.com/attachments/739887563111792731/742812688874209300/giphy.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742812688782065734/tenor.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742812688119496824/4.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742812686969995264/2.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742812687808856104/1.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742812688618619011/5.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742812685389004890/8.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742812685208518676/7.gif',
        ]);

        return new MessageEmbed()
            .setImage(image)
            .setColor(color.EMBED_COLOR)
            .setAuthor(`${message.member?.user.username} грустит TwT`);
    }
}
