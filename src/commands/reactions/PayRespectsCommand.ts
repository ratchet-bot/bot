import { GuildMember, Message, MessageEmbed } from 'discord.js';
import { Arguments, Command } from '../../types/Command';
import color from '../../Palette';
import Utils from '../../Utils';

const commandArgs = {};

export default class BaseReaction implements Command {
    public info = {
        name: 'F',
        trigger: ['F', 'f', 'payrespects', 'payrespect'],
        shortDescription: 'Отдай свой респект!',
        description: 'Команда для того что бы Вы могли отдать честь кому-то',
        args: null,
    };

    async run(message: Message, args: Arguments<typeof commandArgs>, command: string): Promise<any> {
        const raw = message.content.replace(new RegExp('^(?:.*?)' + command, 'gi'), '').trim();
        const image = Utils.random([
            'https://cdn.discordapp.com/attachments/739887563111792731/742784602116063404/tenor.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742784603378680000/giphy.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742784595011043450/5.png',
            'https://cdn.discordapp.com/attachments/739887563111792731/742784594679693352/1534169483128115826.jpg',
            'https://cdn.discordapp.com/attachments/739887563111792731/742784594973163671/1534169515189744448.jpg',
            'https://cdn.discordapp.com/attachments/739887563111792731/742784598504898640/tenor1.gif',
            'https://cdn.discordapp.com/attachments/739887563111792731/742784594352406538/8.png',
        ]);

        return new MessageEmbed()
            .setImage(image)
            .setColor(color.EMBED_COLOR)
            .setDescription(`**${message.member} отдал честь ${Utils.escape(raw)}**`);
    }
}
