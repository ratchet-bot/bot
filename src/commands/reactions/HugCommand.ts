import superagent from 'superagent';
import BaseReaction from '../../types/ReactionBase';
import { GuildMember, Message } from 'discord.js';
import Icons from '../../Icons';
export default class HugCommand extends BaseReaction {
    public info = {
        name: 'hug',
        trigger: ['hug', 'обнять', 'обнимашки'],
        shortDescription: 'Обнять пользователя',
        description: 'С помощью данной команды Вы можете обнять кого-либо.',
        args: { user: { type: GuildMember, required: false, title: 'пользователь' } },
    };
    public aloneUser = '*>.<*';
    public noUser = 'Жалко что у тебя никого нет -b-';
    public text = async (message: Message, user: GuildMember) => {
        return [`${message.author.username} обнимается с ${user.user.username}`, Icons.ACTION_HUG];
    };
    public image = async () => {
        const { body } = await superagent.get(`https://nekos.life/api/v2/img/hug`);
        return body.url;
    };
}
