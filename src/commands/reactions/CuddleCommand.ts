import superagent from 'superagent';
import BaseReaction from '../../types/ReactionBase';
import { GuildMember, Message } from 'discord.js';
import Icons from '../../Icons';
export default class HugCommand extends BaseReaction {
    public info = {
        name: 'cuddle',
        trigger: ['cuddle', 'прижаться', 'прижиматься'],
        shortDescription: 'Прижаться к пользователю',
        description: 'С помощью данной команды Вы можете прижаться к кому-либо.',
        args: { user: { type: GuildMember, required: false, title: 'пользователь' } },
    };
    public aloneUser = '*>.<*';
    public noUser = 'Жалко что у тебя никого нет -b-';
    public text = async (message: Message, user: GuildMember) => {
        return [`${message.author.username} прижимается к ${user.user.username}`, Icons.ACTION_CUDDLE];
    };
    public image = async () => {
        const { body } = await superagent.get(`https://nekos.life/api/v2/img/cuddle`);
        return body.url;
    };
}
