/* eslint-disable @typescript-eslint/no-unused-vars */
// noinspection ES6UnusedImports
import Discord, { Message, MessageEmbed as Embed } from 'discord.js';
import { Arguments, Command, DbInfo } from '../types/Command';
import CommandPermissions from '../types/CommandPermissions';
import { CommandInfo } from '../types/CommandInfo';
import { existsSync } from 'fs';
import path from 'path';
import CommandService, { CommandStatusObject } from '../services/CommandService';
import { CommandStatus } from '../types/CommandStatus';

const commandArgs = {
    file: {
        type: String,
        required: true,
        title: 'файл',
    },
};
export default class TestCommand implements Command {
    public info: CommandInfo = {
        name: 'swap',
        trigger: 'swap',
        shortDescription: 'Перезагрузка модуля',
        description:
            'Длинного описания не будет. Команда перезагружает выбранный модуль. Конец. (Доступно только для разработчиков бота, разумеется)',
        args: commandArgs,
        permission: CommandPermissions.OWNER,
        immutable: true,
    };

    async *run(message: Message, args: Arguments<typeof commandArgs>): AsyncGenerator<any> {
        const filePath = path.join(__dirname, args.file.replace('..', ''));
        if (!existsSync(filePath)) return 'Файл не найден';
        const commandService = await CommandService.build();
        await commandService.swapCommand(filePath, message.client);
        return 'Готово';
    }

    getStatus(): CommandStatusObject {
        if (process.env.NODE_ENV === 'development' && process.argv.includes('--no-swap')) {
            return {
                status: CommandStatus.ENABLED,
            };
        } else {
            if (process.env.NODE_ENV !== 'development')
                return {
                    status: CommandStatus.DISABLED,
                    info: 'Функция доступна только для "INDEV" версии бота',
                };
            if (!process.argv.includes('--no-swap'))
                return {
                    status: CommandStatus.DISABLED,
                    info: 'Включён режим "AUTOSWAP"',
                };
        }
        return {
            status: CommandStatus.DISABLED,
        };
    }
}
