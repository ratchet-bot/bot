import { Arguments, Command, DbInfo } from '../types/Command';
import { Message, MessageEmbed } from 'discord.js';
import Utils from '../Utils';
import DatabaseService from '../services/DatabaseService';
import { ftruncate } from 'fs';

const commandArgs = {
    game: {
        type: String,
        required: true,
    },
};
export default class LeaderboardCommand implements Command {
    public info = {
        name: 'leaderboard',
        trigger: 'leaderboard',
        shortDescription: 'test short',
        description: 'test long',
        args: commandArgs,
        showInHelp: false,
    };

    async run(message: Message, args: Arguments<typeof commandArgs>): Promise<any> {
        const dbService = await DatabaseService.build();
        let game: any;
        if (args.game.match(/^tetri[sz]|тетр[ие][сз]$/i)) {
            game = 'tetris';
        }

        const russianNames = {
            tetris: 'Тетрис',
        };

        const players = await dbService.getGamePlayers('tetris');
        return new MessageEmbed()
            .setTitle('Таблица лидеров игры ' + russianNames[game as keyof typeof russianNames])
            .setDescription(
                '```' +
                    players
                        .sort((a, b) => (b.gameScores.get(game) || 0) - (a.gameScores.get(game) || 0))
                        .map(
                            (p, i) =>
                                i +
                                1 +
                                '. ' +
                                message.client.users.cache.get(p.id)?.username +
                                ' ' +
                                p.gameScores.get('tetris'),
                        )
                        .slice(0, 10)
                        .join('\n') +
                    '```',
            );
    }
}
