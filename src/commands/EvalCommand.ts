/* eslint-disable @typescript-eslint/no-unused-vars */
// noinspection ES6UnusedImports
import Discord, { Message, MessageEmbed as Embed } from 'discord.js';
import { Command, DbInfo } from '../types/Command';
import CommandPermissions from '../types/CommandPermissions';
import { CommandInfo } from '../types/CommandInfo';
import { inspect } from 'util';

// const commandArgs = {};
export default class TestCommand implements Command {
    public info: CommandInfo = {
        name: 'eval',
        trigger: 'eval',
        shortDescription: 'test short',
        description: 'test long',
        args: null,
        permission: CommandPermissions.OWNER,
        showInHelp: false,
        immutable: true,
    };

    async *run(message: Message, args: any, command: string, db: DbInfo): AsyncGenerator<any> {
        const raw = message.content.replace(new RegExp('^(?:.*?)' + command, 'gi'), '');
        let response,
            error = false;
        try {
            await message.delete();
        } catch (e) {}
        try {
            response = await eval(`(async () => {${raw}})();`);
        } catch (e) {
            response = e;
            error = true;
        }
        try {
            await message.author.send(
                inspect(response, {
                    colors: false,
                    showHidden: false,
                    breakLength: 40,
                    depth: 3,
                    maxArrayLength: 50,
                }).substring(0, 5000),
                { split: true, code: 'js' },
            );
        } catch (e) {
            yield 'Не удалось отправить сообщение';
        }
        const msg = yield 'Готово.' + (error ? ' Возвращена ошибка.' : '');
        (msg as Message).delete({ timeout: 3000 }).catch(() => undefined);
    }
}
