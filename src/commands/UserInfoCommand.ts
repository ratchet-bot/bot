import { Arguments, Command } from '../types/Command';
import { GuildMember, Message, MessageEmbed } from 'discord.js';
import Utils from '../Utils';
import m from 'moment';
import Bot from '../Bot';
import Emojis from '../types/Emojis';
import color from '../Palette';
import Icons from '../Icons';
import { CommandInfo } from '../types/CommandInfo';
import formatDate = Utils.formatDate;
import pastDuration = Utils.pastDuration;
import TextEscape = Utils.TextEscape;
import EmojisEscape = Utils.EmojisEscape;

m.locale('ru');

const commandArgs = {
    user: {
        type: GuildMember,
        required: false,
        title: 'пользователь',
    },
};
export default class UserInfoCommand implements Command {
    public info: CommandInfo = {
        name: 'userinfo',
        trigger: [/^user(info?)?(rmation)?$/i, /^юзер(инфо)?$/i],
        aliases: ['userinfo', 'юзеринфо'],
        shortDescription: 'Инфромация о пользователе',
        description: 'С помощью этой команды Вы можете посмотреть информацию о себе, или о другом пользователе',
        args: commandArgs,
    };

    async run(message: Message, args: Arguments<typeof commandArgs>): Promise<any> {
        const user = args.user || message.member;
        const customState = message.guild?.members.cache
            .get(user.id)
            ?.presence.activities.find((e) => e.type === 'CUSTOM_STATUS')?.state;

        const embed = new MessageEmbed()
            .setAuthor(
                Utils.esc(true, EmojisEscape.KEEP_EMOJIS, TextEscape.REMOVE, EmojisEscape.REMOVE_EMOJIS)`${
                    user.user.bot ? 'Бот' : 'Пользователь'
                } ${user.user.tag}`,
                user.user.bot ? `${Icons.BOT}` : `${Icons.TWOUSER}`,
            )
            .setColor(color.GRAY)
            .setThumbnail(user.user.displayAvatarURL({ size: 4096 }) || 'undefined')
            .setDescription(
                (customState
                    ? Utils.esc(
                          true,
                          EmojisEscape.KEEP_EMOJIS,
                          TextEscape.IN_CODE_BLOCK,
                          EmojisEscape.REMOVE_EMOJIS,
                      )`Пользовательский статус: **\`\`\`fix\n${customState}\`\`\`**\n`
                    : '') +
                    {
                        online: Emojis.ONLINE + ' Онлайн',
                        dnd: Emojis.DND + ' Не беспокоить',
                        idle: Emojis.IDLE + ' Нет на месте',
                        offline: Emojis.OFF + ' Оффлайн',
                    }[user.user.presence.status] +
                    `${Utils.list(
                        [
                            user.user.presence.clientStatus?.desktop && 'компьютере',
                            user.user.presence.clientStatus?.mobile && 'телефоне',
                            user.user.presence.clientStatus?.web && 'сайте',
                        ]
                            .filter(Boolean)
                            .map((e, i) => (!i ? ' на ' : '') + e) as string[],
                    )}` +
                    '\n' +
                    `Цвет ника: ${user?.displayHexColor === '#000000' ? 'По умолчанию' : user?.displayHexColor}`,
            )
            .addField(
                `Самая высшая роль • Всего ролей`,
                `${
                    user.roles.cache.size > 1
                        ? `${user.roles.highest} • ${user.roles.cache.size - 1} ${Utils.decForInt(
                              user.roles.cache.size - 1,
                              [`роль`, `роли`, `ролей`],
                          )}`
                        : 'Нет ролей'
                }`,
            )
            .addField(
                user.user.bot ? `:door: Дата подключения на сервер` : `:door: Присоединился`,
                `${formatDate(user?.joinedAt)}\n${pastDuration(user?.joinedAt)} назад`,
                true,
            )
            .addField(
                user.user.bot ? `:calendar_spiral: Был создан` : `:pencil: Зарегистрировался`,
                `${formatDate(user.user.createdAt)}\n${pastDuration(user.user.createdAt)} назад`,
            );

        if (message.guild?.ownerID == user.id) {
            embed.setAuthor(Utils.e`Владелец сервера ${user.user.tag}`, Icons.OWNER);
        }
        if (Bot.owners.includes(user.id)) {
            embed.setAuthor(Utils.e`Разработчик бота ${user.user.tag}`, Icons.USER_RANK_DEV);
            embed.setColor(color.BLUE_DEV);
        }

        return embed;
    }
}
