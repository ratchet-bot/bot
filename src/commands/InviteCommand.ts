import { Command } from '../types/Command';
import { Message, MessageEmbed } from 'discord.js';
import config from '../config';
import Icons from '../Icons';
import color from '../Palette';
import { CommandInfo } from '../types/CommandInfo';

const commandArgs = {};
export default class InviteCommand implements Command {
    public info: CommandInfo = {
        name: 'invite',
        trigger: [/^invite$/i, /^инва[йи]т$/i],
        aliases: ['invite', 'инвайт'],
        shortDescription: 'Пригласить бота',
        description:
            'С помощью этой команды Вы сможете пригласить бота на свой сервер, а так же зайти на сервер поддержки.',
        args: commandArgs,
    };

    async run(message: Message): Promise<any> {
        return new MessageEmbed()
            .setColor(color.YELLOW)
            .setAuthor(`Ссылки бота`, `${Icons.LINK_YELLOW}`)
            .setDescription(
                `ㆍ[Пригласить бота](${await message.client.generateInvite(['ADMINISTRATOR'])})\n` +
                    `ㆍ~~Пожертвовать деньги на развитие~~\n` +
                    `ㆍ[Техническая поддержка бота](https://discord.gg/QTmCv44)\n` +
                    `ㆍРазработчики бота: ${config.OWNER.map(
                        (u) => '`' + message.client.users.cache.get(u)?.tag + '`',
                    ).join(', ')}`,
            );
    }
}
