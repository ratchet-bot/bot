import { Arguments, Command } from '../../types/Command';
import { Message, User } from 'discord.js';
import Icons from '../../Icons';
import color from '../../Palette';
import PermissionError from '../../errors/PermissionError';
import { Action } from '../../objects/Action';
import { MessageError } from '../../errors/MessageError';
import { CommandInfo } from '../../types/CommandInfo';

const commandArgs = {
    user: {
        type: User,
        required: true,
        title: 'пользователь',
    },
    reason: {
        type: String,
        required: false,
        title: 'причина',
    },
};
export default class UnbanCommand implements Command {
    public info: CommandInfo = {
        name: 'unban',
        trigger: [/^(un|ан|-|раз)(ban|[бьъ]ан(чик)?|банить?)$/i],
        aliases: ['unban', 'разбанить', '-ban'],
        shortDescription: 'Разбанить пользователя',
        description: 'С помощью этой команды вы можете разбанить кого-либо.',
        permission: 'BAN_MEMBERS',
        botPermission: 'BAN_MEMBERS',
        args: commandArgs,
    };

    async run(message: Message, args: Arguments<typeof commandArgs>): Promise<any> {
        const bans = await message.guild?.fetchBans();
        if (!bans?.has(args.user.id)) throw new MessageError('Пользователь не забанен');
        try {
            await message.guild?.members.unban(
                args.user,
                'Администратор: ' + message.author + (args.reason ? ' Причина: ' + args.reason : ''),
            );
        } catch (e) {
            if ('code' in e && e.code === 50013)
                throw new PermissionError('У бота нет прав разбанить этого пользователя');
        }
        return new Action({
            title: 'Пользователь был разбанен',
            icon: Icons.USER_JOIN,
            moderator: message.author,
            user: args.user,
            reason: args.reason || 'не указана',
            color: color.ADD,
        });
    }
}
