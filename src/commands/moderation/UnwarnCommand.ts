import { Arguments, Command } from '../../types/Command';
import { Message } from 'discord.js';
import Icons from '../../Icons';
import color from '../../Palette';
import { Action } from '../../objects/Action';
import DatabaseService from '../../services/DatabaseService';
import { MessageError } from '../../errors/MessageError';
import { CommandInfo } from '../../types/CommandInfo';

const commandArgs = {
    warn: {
        type: Number,
        required: true,
        title: 'номер варна',
    },
    reason: {
        type: String,
        required: false,
        title: 'причина',
    },
};
export default class UnwarnCommand implements Command {
    public info: CommandInfo = {
        name: 'unwarn',
        trigger: [/^(un|ан|-|раз|от)(warn|варн|пред)(ить)?$/i],
        aliases: ['unwarn', 'разварнить', '-пред'],
        shortDescription: 'Снятие варнов',
        description: 'Снятие варнов с пользователя.',
        permission: ['MANAGE_MESSAGES', ['KICK_MEMBERS', 'BAN_MEMBERS']],
        args: commandArgs,
    };

    async run(message: Message, args: Arguments<typeof commandArgs>): Promise<any> {
        const dbService = await DatabaseService.build();
        const data = await dbService.getWarn(args.warn);
        if (!data) throw new MessageError('Предупреждение не найдено');
        const { warn, member } = data;
        await member.unWarn(warn.id);
        return new Action({
            id: warn.id,
            title: 'Отозвано предупреждение',
            icon: Icons.UNWARNED,
            color: color.ADD,
            moderator: message.author,
            user: await message.client.users.fetch(member.userId),
            reason: args.reason,
        });
    }
}
