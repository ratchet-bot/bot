import { Arguments, Command, DbInfo } from '../../types/Command';
import { GuildMember, Message, MessageEmbed } from 'discord.js';
import Icons from '../../Icons';
import color from '../../Palette';
import { Action } from '../../objects/Action';
import DatabaseService from '../../services/DatabaseService';
import { CommandInfo } from '../../types/CommandInfo';
import moment from 'moment';
import Utils from '../../Utils';
import formatDate = Utils.formatDate;

moment.locale('ru');

const commandArgs = {
    member: {
        type: GuildMember,
        required: false,
        title: 'пользователь',
    },
};
export default class WarnCommand implements Command {
    public info: CommandInfo = {
        name: 'warn',
        trigger: [/^\+?warn(s|list)$/i, /^варн(ы|лист)?/i],
        aliases: ['варны', 'warns', 'warnlist'],
        shortDescription: 'Выдача варнов',
        description: 'Выдача варнов пользователю.',
        permission: ['MANAGE_MESSAGES', ['KICK_MEMBERS', 'BAN_MEMBERS']],
        args: commandArgs,
    };

    async run(message: Message, args: Arguments<typeof commandArgs>, command: string, db: DbInfo): Promise<any> {
        if (!message.member) return;
        const dbService = await DatabaseService.build();
        const dbMember = await dbService.getDbMember(args.member ?? message.member);
        return new MessageEmbed()
            .setAuthor(
                'Варны пользователя ' + (args.member ?? message.member).user.tag,
                (args.member ?? message.member).user.avatarURL({ format: 'webp', size: 1024 }) ?? undefined,
            )
            .setDescription(
                dbMember.warns
                    .slice(dbMember.warns.length - 10, dbMember.warns.length)
                    .map(
                        (w) =>
                            `Варн #${w.id}\n- Модератор: <@${w.moderator}>\n- Дата: ${formatDate(
                                w.time * 1000,
                            )}\n- Причина: ${w.reason}`,
                    )
                    .join('\n\n') || 'Список пуст',
            );
    }
}
