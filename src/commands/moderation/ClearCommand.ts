import { Arguments, Command } from '../../types/Command';
import { GuildChannel, Message } from 'discord.js';
import Icons from '../../Icons';
import color from '../../Palette';
import { Action } from '../../objects/Action';
import Utils from '../../Utils';
import { CommandInfo } from '../../types/CommandInfo';

const commandArgs = {
    count: {
        type: Number,
        required: true,
        title: 'кол-во сообщений',
    },
    reason: {
        type: String,
        required: false,
        title: 'причина',
    },
};
export default class ClearCommand implements Command {
    public info: CommandInfo = {
        name: 'clear',
        trigger: [/^cl(ea|i)[rn]$/i, /^очист(ить|(оч)?ка)$/i],
        aliases: ['clear', 'clean', 'очистить'],
        shortDescription: 'Очистка канала',
        description: 'С помощью этой команды Вы можете очистить указанное кол-во сообщений в чате.',
        permission: 'MANAGE_MESSAGES',
        botPermission: 'MANAGE_MESSAGES',
        args: commandArgs,
    };

    async *run(message: Message, args: Arguments<typeof commandArgs>): AsyncGenerator<any> {
        let count = args.count;
        while (count > 0) {
            await message.channel.bulkDelete(count >= 100 ? 100 : count);
            count -= count >= 100 ? 100 : count;
        }
        const msg = yield new Action({
            title: `Очищено ${args.count} ${Utils.decForInt(args.count, ['сообщение', 'сообщения', 'сообщений'])}`,
            icon: Icons.MESSAGE_BUKLDELETE,
            moderator: message.author,
            channel: message.channel as GuildChannel,
            color: color.REMOVE,
            reason: args.reason,
        });
        try {
            (msg as Message).delete({ timeout: 10000 }).catch(() => null);
        } catch (e) {}
    }
}
