import { Arguments, Command } from '../../types/Command';
import { Message, User } from 'discord.js';
import Icons from '../../Icons';
import color from '../../Palette';
import { Action } from '../../objects/Action';
import PermissionError from '../../errors/PermissionError';
import { CommandInfo } from '../../types/CommandInfo';

const commandArgs = {
    user: {
        type: User,
        required: true,
        title: 'пользователь',
    },
    reason: {
        type: String,
        required: false,
        title: 'причина',
    },
};
export default class BanCommand implements Command {
    public info: CommandInfo = {
        name: 'ban',
        trigger: [/^\+?ban$/i, /^\+?[бъь]ан(чик)?$/i],
        aliases: ['бан', 'ban'],
        shortDescription: 'Забанить пользователя',
        description: 'С помощью этой команды Вы можете забанить кого-либо.',
        permission: 'BAN_MEMBERS',
        botPermission: 'BAN_MEMBERS',
        args: commandArgs,
    };

    async run(message: Message, args: Arguments<typeof commandArgs>): Promise<any> {
        try {
            await message.guild?.members.ban(args.user.id, {
                reason: 'Администратор: ' + message.author + (args.reason ? ' Причина: ' + args.reason : ''),
            });
        } catch (e) {
            if ('code' in e && e.code === 50013)
                throw new PermissionError('У бота нет прав забанить этого пользователя');
        }
        return new Action({
            title: 'Пользователь был забанен',
            icon: Icons.USER_LEAVE,
            moderator: message.author,
            user: args.user,
            reason: args.reason || 'не указана',
            color: color.REMOVE,
        });
    }
}
