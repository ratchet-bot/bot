import { Arguments, Command, DbInfo } from '../../types/Command';
import { Guild, GuildChannel, GuildMember, Message, PartialGuildMember, User } from 'discord.js';
import { CommandInfo } from '../../types/CommandInfo';
import DatabaseService from '../../services/DatabaseService';
import { MessageError } from '../../errors/MessageError';
import moment from 'moment';
import color from '../../Palette';
import { Action } from '../../objects/Action';
import Icons from '../../Icons';
import MessageService from '../../services/MessageService';
import { CommandStatusObject } from '../../services/CommandService';
import { CommandStatus } from '../../types/CommandStatus';

moment.locale('ru');

const commandArgs = {
    member: {
        type: GuildMember,
        required: true,
        title: 'пользователь',
    },
    reason: {
        type: String,
        required: false,
        title: 'причина',
    },
    time: {
        type: String,
        required: false,
        title: 'время',
    },
};

export default class MuteCommand implements Command {
    public info: CommandInfo = {
        name: 'mute',
        trigger: [/^\+?mute?$/i, /^\+?мут[еэ]?$/i, /^з[ао]му(тить|чать)$/i],
        aliases: ['mute', 'мут', 'замутить'],
        shortDescription: 'Замутить пользователя',
        description:
            'С помощью этой команды Вы можете ограничить пользователю отправку сообщений в чат до отмены, или на указанное время.',
        permission: ['MANAGE_MESSAGES', 'MUTE_MEMBERS'],
        botPermission: 'MANAGE_ROLES',
        args: commandArgs,
    };

    public handlers = {
        guildMemberUpdate: [this.guildMemberUpdate.bind(this)],
        guildMemberAdd: [this.guildMemberAdd.bind(this)],
    };

    async guildMemberAdd(member: PartialGuildMember | GuildMember) {
        const dbService = await DatabaseService.build();
        const dbMember = await dbService.getDbMember(member as GuildMember);
        const dbGuild = await dbService.getDbGuild(member.guild);
        if (dbMember.muted !== 0 && dbGuild.settings.mute.enabled && dbGuild.settings.mute.roleId)
            await member.roles.add(dbGuild.settings.mute.roleId);
    }

    async guildMemberUpdate(_old: PartialGuildMember | GuildMember, _new: PartialGuildMember | GuildMember) {
        const dbService = await DatabaseService.build();
        const dbGuild = await dbService.getDbGuild(_old.guild);
        if (!dbGuild.settings.mute.enabled || !dbGuild.settings.mute.roleId) return;
        if (
            !(!_old.roles.cache.has(dbGuild.settings.mute.roleId) && _new.roles.cache.has(dbGuild.settings.mute.roleId))
        )
            return;

        let moderator;
        try {
            const audit = await _old.guild.fetchAuditLogs({
                type: 'MEMBER_ROLE_UPDATE',
                limit: 10,
            });
            moderator = audit.entries.find((e) => {
                return (
                    (e.target as User)?.id === _old.id &&
                    (e.changes || [])?.[0]?.key === '$add' &&
                    (e.changes || [])?.[0]?.new?.[0]?.id === dbGuild.settings.mute.roleId
                );
            })?.executor;
        } catch (e) {}

        const member = await _new.fetch();
        const dbMember = await dbService.getDbMember(member);
        await dbMember.mute(-1);

        await MessageService.resolveAction(
            _new.guild,
            new Action({
                title: 'Пользователь замучен',
                icon: Icons.USER_MUTED,
                color: color.REMOVE,
                moderator,
                user: member.user,
                reason: 'Была добавлена роль мута',
            }),
        );
    }

    async run(message: Message, args: Arguments<typeof commandArgs>, command: string, db: DbInfo): Promise<any> {
        if (!message.member) return;
        if (!db.guild.settings.mute.enabled || !db.guild.settings.mute.roleId)
            throw new MessageError('Мут отключен на этом сервере');

        const dbService = await DatabaseService.build();

        try {
            await args.member.roles.add(
                db.guild.settings.mute.roleId,
                `Администратор: ${message.author.tag}${args.reason ? ` Причина: ${args.reason}` : ''}`,
            );
        } catch (e) {
            throw new MessageError('Не могу дать роль...');
        }

        const time = args.time
            ? moment().add(moment.duration('PT' + args.time.toUpperCase().replace(/-/g, '')))
            : undefined;

        const member = await dbService.getDbMember(args.member);
        await member.mute(time && time.unix() ? time.unix() : -1);

        return new Action({
            title: 'Пользователь замучен',
            icon: Icons.USER_MUTED,
            color: color.REMOVE,
            user: args.member.user,
            moderator: message.author,
            reason: args.reason,
            time: time && time.unix() ? (time.unix() - moment().unix()) * 1000 : undefined,
        });
    }

    getStatus(
        message: Message | { guild: Guild; member: GuildMember; channel: GuildChannel },
        db: DbInfo,
    ): CommandStatusObject {
        const settings = db.guild.settings.mute;
        if (!settings.enabled) return { status: CommandStatus.DISABLED, info: 'Мут отключён администратором.' };
        if (!settings.roleId)
            return {
                status: CommandStatus.NEEDS_ACTION,
                info: 'Нужно установить роль мута.\n(Используйте команду settings)',
            };
        if (!message.guild?.roles.resolve(settings.roleId))
            return { status: CommandStatus.NEEDS_ACTION, info: 'Роль мута не существует.' };
        return { status: CommandStatus.ENABLED };
    }
}
