import { Arguments, Command, DbInfo } from '../../types/Command';
import { Client, Guild, GuildChannel, GuildMember, Message, PartialGuildMember, User } from 'discord.js';
import { CommandInfo } from '../../types/CommandInfo';
import DatabaseService from '../../services/DatabaseService';
import { MessageError } from '../../errors/MessageError';
import moment from 'moment';
import color from '../../Palette';
import { Action } from '../../objects/Action';
import Icons from '../../Icons';
import MessageService from '../../services/MessageService';
import Timeout = NodeJS.Timeout;
import { setInterval, clearInterval } from 'timers';
import { CommandStatusObject } from '../../services/CommandService';
import { CommandStatus } from '../../types/CommandStatus';

moment.locale('ru');

const commandArgs = {
    member: {
        type: GuildMember,
        required: true,
        title: 'пользователь',
    },
    reason: {
        type: String,
        required: false,
        title: 'причина',
    },
};

export default class MuteCommand implements Command {
    private interval?: Timeout;

    public info: CommandInfo = {
        name: 'unmute',
        trigger: [/^(un|ан|-|раз)(mute?|мут[еэ]?|му(тить|чать))$/i],
        aliases: ['размут', 'unmute', '-mute'],
        shortDescription: 'Размутить пользователя',
        description: 'С помощью этой команды вы можете вновь разрешить пользователю писать сообщения.',
        permission: ['MANAGE_MESSAGES', 'MUTE_MEMBERS'],
        botPermission: 'MANAGE_ROLES',
        args: commandArgs,
    };

    public handlers = {
        guildMemberUpdate: [this.guildMemberUpdate.bind(this)],
    };

    async guildMemberUpdate(_old: PartialGuildMember | GuildMember, _new: PartialGuildMember | GuildMember) {
        const dbService = await DatabaseService.build();
        const dbGuild = await dbService.getDbGuild(_old.guild);
        if (!dbGuild.settings.mute.enabled || !dbGuild.settings.mute.roleId) return;
        if (
            !(_old.roles.cache.has(dbGuild.settings.mute.roleId) && !_new.roles.cache.has(dbGuild.settings.mute.roleId))
        )
            return;

        let moderator;
        try {
            const audit = await _old.guild.fetchAuditLogs({
                type: 'MEMBER_ROLE_UPDATE',
                limit: 10,
            });
            moderator = audit.entries.find((e) => {
                return (
                    (e.target as User)?.id === _old.id &&
                    (e.changes || [])?.[0]?.key === '$remove' &&
                    (e.changes || [])?.[0]?.new?.[0]?.id === dbGuild.settings.mute.roleId
                );
            })?.executor;
        } catch (e) {}

        const member = await _new.fetch();
        const dbMember = await dbService.getDbMember(member);
        await dbMember.mute(0);

        await MessageService.resolveAction(
            _new.guild,
            new Action({
                title: 'Пользователь размучен',
                icon: Icons.USER_UNMUTE,
                color: color.ADD,
                moderator,
                user: member.user,
                reason: 'Роль мута была снята',
            }),
        );
    }

    async run(message: Message, args: Arguments<typeof commandArgs>, command: string, db: DbInfo): Promise<any> {
        if (!message.member) return;
        if (!db.guild.settings.mute.enabled || !db.guild.settings.mute.roleId)
            throw new MessageError('Мут отключен на этом сервере');

        const dbService = await DatabaseService.build();

        try {
            await args.member.roles.remove(db.guild.settings.mute.roleId);
        } catch (e) {
            throw new MessageError('Не могу снять роль...');
        }

        const member = await dbService.getDbMember(args.member);
        await member.mute(0);
        return new Action({
            title: 'Пользователь размучен',
            icon: Icons.USER_UNMUTE,
            color: color.ADD,
            moderator: message.author,
            user: args.member.user,
            reason: args.reason,
        });
    }

    async unmuteUsers(client: Client) {
        const dbService = await DatabaseService.build();
        const membersToUnmute = await dbService.getDbMembersToUnmute(moment().unix());

        for (const dbMember of membersToUnmute) {
            const guild = client.guilds.resolve(dbMember.guildId);
            if (!guild) return;

            const dbGuild = await dbService.getDbGuild(guild);
            if (!dbGuild.settings.mute.roleId) return dbMember.mute(0);

            const member = await guild.members.fetch(dbMember.userId);
            if (!member) await dbMember.mute(0);

            await MessageService.resolveAction(
                guild,
                new Action({
                    title: 'Пользователь размучен',
                    icon: Icons.USER_UNMUTE,
                    color: color.ADD,
                    moderator: client.user as User,
                    user: member.user,
                    reason: 'Автоматический размут',
                }),
            );

            await member.roles.remove(dbGuild.settings.mute.roleId);
            // todo try/catch

            await dbMember.mute(0);
        }
    }

    onLoaded(client: Client) {
        this.interval = setInterval(() => this.unmuteUsers(client), 5000);
    }

    beforeUnloaded() {
        if (this.interval) clearInterval(this.interval);
    }

    getStatus(
        message: Message | { guild: Guild; member: GuildMember; channel: GuildChannel },
        db: DbInfo,
    ): CommandStatusObject {
        const settings = db.guild.settings.mute;
        if (!settings.enabled) return { status: CommandStatus.DISABLED, info: 'Мут отключён администратором.' };
        if (!settings.roleId)
            return {
                status: CommandStatus.NEEDS_ACTION,
                info: 'Нужно установить роль мута.\n(Используйте команду settings)',
            };
        if (!message.guild?.roles.resolve(settings.roleId))
            return { status: CommandStatus.NEEDS_ACTION, info: 'Роль мута не существует.' };
        return { status: CommandStatus.ENABLED };
    }
}
