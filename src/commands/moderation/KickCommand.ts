import { Arguments, Command } from '../../types/Command';
import { GuildMember, Message } from 'discord.js';
import Icons from '../../Icons';
import color from '../../Palette';
import PermissionError from '../../errors/PermissionError';
import { Action } from '../../objects/Action';
import { CommandInfo } from '../../types/CommandInfo';

const commandArgs = {
    member: {
        type: GuildMember,
        required: true,
        title: 'пользователь',
    },
    reason: {
        type: String,
        required: false,
        title: 'причина',
    },
};
export default class KickCommand implements Command {
    public info: CommandInfo = {
        name: 'kick',
        trigger: [/^\+?kic?k$/i, /^\+?кик$/i, /^выкинуть$/i],
        shortDescription: 'Кикнуть пользователя',
        description: 'С помощью этой команды вы можете исключить пользователя с сервера.',
        permission: 'KICK_MEMBERS',
        botPermission: 'KICK_MEMBERS',
        args: commandArgs,
    };

    async run(message: Message, args: Arguments<typeof commandArgs>): Promise<any> {
        try {
            await args.member.kick(
                `Администратор: ${message.author.username}${args.reason ? ' Причина: ' + args.reason : ''}`,
            );
        } catch (e) {
            if ('code' in e && e.code === 50013)
                throw new PermissionError('У бота нет прав забанить этого пользователя');
        }
        return new Action({
            title: 'Пользователь был выгнан',
            color: color.REMOVE,
            icon: Icons.USER_LEAVE,
            moderator: message.author,
            user: args.member.user,
        });
    }
}
