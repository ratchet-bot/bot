import { Arguments, Command, DbInfo } from '../../types/Command';
import { GuildMember, Message } from 'discord.js';
import Icons from '../../Icons';
import color from '../../Palette';
import { Action } from '../../objects/Action';
import DatabaseService from '../../services/DatabaseService';
import { CommandInfo } from '../../types/CommandInfo';

const commandArgs = {
    member: {
        type: GuildMember,
        required: true,
        title: 'пользователь',
    },
    reason: {
        type: String,
        required: false,
        title: 'причина',
    },
};
export default class WarnCommand implements Command {
    public info: CommandInfo = {
        name: 'warn',
        trigger: [/^\+?warn$/i, /^(\+|за)?варн(ить)?/i, /^\+?пред$/i],
        shortDescription: 'Выдача варнов',
        description: 'Выдача варнов пользователю.',
        permission: ['MANAGE_MESSAGES', ['KICK_MEMBERS', 'BAN_MEMBERS']],
        args: commandArgs,
    };

    async run(message: Message, args: Arguments<typeof commandArgs>, command: string, db: DbInfo): Promise<any> {
        const dbService = await DatabaseService.build();
        const member = await dbService.getDbMember(args.member);
        const warn = await member.warn(db.guild, message.author.id, args.reason);
        return new Action({
            id: warn.id,
            title: 'Выдано предупреждение',
            icon: Icons.WARNED,
            color: color.REMOVE,
            moderator: message.author,
            user: args.member.user,
            reason: args.reason,
        });
    }
}
