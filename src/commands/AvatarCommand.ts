import { Arguments, Command } from '../types/Command';
import { GuildMember, Message, MessageEmbed } from 'discord.js';
import config from '../config';
import Utils from '../Utils';
import Ico from '../Icons';
import color from '../Palette';
import { CommandInfo } from '../types/CommandInfo';

const commandArgs = {
    user: {
        type: GuildMember,
        required: false,
        title: 'пользователь',
    },
};
export default class AvatarCommand implements Command {
    public info: CommandInfo = {
        name: 'avatar',
        trigger: [/^фото((видео)?карточка)?$/i, /^ава(тар(ка)?)?$/i, /av(a(tar)?)?/i],
        aliases: ['avatar', 'аватар', 'фото'],
        shortDescription: 'Посмотреть аватарку',
        description: 'С помощью этой команды Вы можете посмотреть свою или чью-то аватарку.',
        args: commandArgs,
    };

    run(message: Message, args: Arguments<typeof commandArgs>): any {
        const user = args.user || message.member;
        const embed = new MessageEmbed()
            .setColor(color.GRAY)
            .setDescription(
                `Не видна аватарка? [Нажмите вот сюда](${
                    user.user.avatarURL({ size: 4096 }) || user.user.defaultAvatarURL
                })`,
            )
            .setImage(user.user.avatarURL({ size: 4096 }) || user.user.defaultAvatarURL)
            .setAuthor(
                `${args.user ? Utils.e`Аватарка ${user.user.bot ? 'бота ' : ''}${user.user.tag}` : 'Ваша аватарка'}`,
                user.user.bot ? Ico.BOT : Ico.TWOUSER,
            );

        if (message.guild?.ownerID == user.id) {
            embed.setAuthor(Utils.e`Аватарка владельца ${user.user.tag}`, `${Ico.OWNER}`);
        }
        if (config.OWNER.includes(user.id)) {
            embed.setAuthor(`Аватарка разработчика бота`, `${Ico.USER_RANK_DEV}`);
            embed.setColor(color.BLUE_DEV);
        }

        return embed;
    }
}
