import { Arguments, Command, DbInfo } from '../types/Command';
import { Message, MessageEmbed } from 'discord.js';
import Utils from '../Utils';
import color from '../Palette';
import Icons from '../Icons';
import config from '../config';

const commandArgs = {
    args: { type: String, required: true },
};
export default class TestCommand implements Command {
    public info = {
        name: 'BotMe',
        trigger: 'BotMe',
        shortDescription: 'Сделать себя "ботом"',
        description: 'Команда для отправки вебхука с именем и аватаркой пользователя',
        args: commandArgs,
        showInHelp: false,
    };

    async run(message: Message): Promise<any> {
        if (message.channel.type !== 'text') return;

        await message.channel.createWebhook(`RatchetBotㆍBotMe webhook`, {
                avatar: message.client.user?.avatarURL({ size: 4096 }) || undefined,
                reason: `BotMe Command execution.`,
            })
            .then((wb) => console.log(wb.id));
    }
}
