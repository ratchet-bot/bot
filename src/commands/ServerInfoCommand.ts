import { Arguments, Command, DbInfo } from '../types/Command';
import { Message, MessageEmbed } from 'discord.js';
import m from 'moment';
import Utils from '../Utils';
import Emojis from '../types/Emojis';
import Icons from '../Icons';
import color from '../Palette';
import TextEscape = Utils.TextEscape;
import EmojisEscape = Utils.EmojisEscape;
import { CommandInfo } from '../types/CommandInfo';

m.locale('ru');

const commandArgs = {};

export default class ServerInfoCommand implements Command {
    public info: CommandInfo = {
        name: 'serverinfo',
        trigger: [/^сервер(ная)?_?(инф[оа]?рмация)$/i, /^(server|guild)_?(inf[oa]?(rmation)?)?$/i],
        aliases: ['сервер', 'серверинфо', 'server', 'guild', 'serverinfo'],
        shortDescription: 'Информация о сервере',
        description: 'С помощью этой команды Вы можете узнать расширенную информацию о сервере.',
        args: commandArgs,
    };

    static VERIFICATION_LEVELS = {
        NONE: 'отсутствует',
        LOW: 'низкий',
        MEDIUM: 'средний',
        HIGH: 'высокий',
        VERY_HIGH: 'максимальный',
    };

    static REGIONS = {
        europe: '🇪🇺 Европа ',
        singapore: '🇸🇬 Сингапур',
        russia: '🇷🇺 Россия',
        'us-west': '🇺🇸 США (Запад)',
        'us-east': '🇺🇸 США (Восток)',
        'us-central': '🇺🇸 США (Центр)',
        'us-south': '🇺🇸 США (Юг)',
        southafrica: '🇿🇦 Ю.Африка',
        japan: '🇯🇵 Япония',
        hongkong: '🇭🇰 Хонг Конд',
        sydney: '🇦🇺 Сидей',
        brazil: '🇧🇷 Бразилия',
        india: '🇮🇳 Индия',
    };

    list(elements: (string | false | null | undefined | 0)[]): string | false {
        return elements.filter(Boolean).length ? elements.filter(Boolean).join('\n') : false;
    }

    membersWithStatus(message: Message, status: string): number {
        return message.guild?.members.cache.filter((m) => m.user.presence.status === status)?.size || 0;
    }

    async run(message: Message, args: Arguments<typeof commandArgs>, command: string, db: DbInfo): Promise<any> {
        // todo отступы в пунктах списков
        if (!message.guild) return;
        const embed = new MessageEmbed()
            .setColor(color.GRAY)
            .setAuthor(
                Utils.esc(
                    true,
                    EmojisEscape.KEEP_EMOJIS,
                    TextEscape.REMOVE,
                    EmojisEscape.REMOVE_EMOJIS,
                )`Сервер ${message.guild?.name}`,
                `${Icons.GUILD}`,
            )
            .setThumbnail(message.guild?.iconURL({ size: 4096 }) || '');
        const serverInfoPerms = db.guild.settings.privacy.serverInfo;

        if (serverInfoPerms?.get('owner') !== false)
            embed.setDescription(`${message.guild.owner} является владельцем сервера`);

        const mainList = this.list([
            // '\u200b',
            serverInfoPerms?.get('region') !== false &&
                `Регион:\n• \`${
                    ServerInfoCommand.REGIONS[message.guild.region as keyof typeof ServerInfoCommand.REGIONS]
                }\``,
            serverInfoPerms?.get('verificationLevel') !== false &&
                `Уровень проверки:\n• \`${ServerInfoCommand.VERIFICATION_LEVELS[message.guild.verificationLevel]}\``,
            serverInfoPerms?.get('creationDate') !== false &&
                `Дата создания:\n• \`${m(message.guild.createdAt).format('Do MMM YYYY')}\``,
        ]);
        if (mainList) embed.addField(/*`:gear: Основное`*/ '\u200b', mainList, true);

        // if (serverInfoPerms?.get('channels') !== false) {
        //     const text = message.guild.channels.cache.filter((c) => c.type === 'text').size;
        //     const voice = message.guild.channels.cache.filter((c) => c.type === 'text').size;
        //     embed.addField(
        //         `${Emojis.CHAT} Всего каналов ${message.guild.channels.cache.size}`,
        //         `${Emojis.CHANNEL} ${text} ${Utils.decForInt(text, ['текстовый', 'текстовых', 'текстовых'])}\n` +
        //             `${Emojis.VOICE} ${voice} ${Utils.decForInt(voice, ['голосовой', 'голосовых', 'голосовых'])}`,
        //         true,
        //     );
        // }
        //
        // const bots = message.guild.members.cache.filter((m) => m.user.bot).size;
        // embed.addField(
        //     `${Emojis.USERS} ${message.guild.memberCount} ${Utils.decForInt(message.guild.memberCount, [
        //         'участник',
        //         'участника',
        //         'участников',
        //     ])}`,
        //     `${Emojis.USER} ${Utils.decForInt(message.guild.memberCount - bots, ['человек', 'человека', 'людей'])}\n` +
        //         `${Emojis.BOT} ${Utils.decForInt(bots, ['бот', 'бота', 'ботов'])}`,
        //     true,
        // );

        const totalChannels = message.guild.channels.cache.size;
        const news = message.guild.channels.cache.filter((c) => c.type === 'news').size;
        const stores = message.guild.channels.cache.filter((c) => c.type === 'store').size;
        const text = message.guild.channels.cache.filter((c) => c.type === 'text').size;
        const voice = message.guild.channels.cache.filter((c) => c.type === 'voice').size;
        const cats = message.guild.channels.cache.filter((c) => c.type === 'category').size;
        const totalMembers = message.guild.memberCount;
        const bots = message.guild.members.cache.filter((m) => m.user.bot).size;
        const members = totalMembers - bots;
        const summaryList = this.list([
            // '\u200b',
            `${Emojis.USERS} **${totalMembers} ${Utils.decForInt(totalMembers, [
                'пользователь',
                'пользователя',
                'пользователей',
            ])}:**`,
            `_ _ ${Emojis.USER} ${members} ${Utils.decForInt(members, ['человек', 'человека', 'людей'])}`,
            `_ _ ${Emojis.BOT} ${bots} ${Utils.decForInt(bots, ['бот', 'бота', 'ботов'])}`,
            `\n${Emojis.CHAT} **${totalChannels} ${Utils.decForInt(totalChannels, ['канал', 'канала', 'каналов'])}:**`,
            serverInfoPerms?.get('channels') !== false &&
                serverInfoPerms?.get('channels') !== false &&
                cats &&
                `_ _ ${Emojis.CATEGORY} ${cats} ${Utils.decForInt(cats, ['категория', 'категории', 'категорий'])}`,
            serverInfoPerms?.get('channels') !== false &&
                text &&
                `_ _ ${Emojis.CHANNEL} ${text} ${Utils.decForInt(text, ['текстовый', 'текстовых', 'текстовых'])}`,
            serverInfoPerms?.get('channels') !== false &&
                voice &&
                `_ _ ${Emojis.VOICE} ${voice} ${Utils.decForInt(voice, ['голосовой', 'голосовых', 'голосовых'])}`,
            serverInfoPerms?.get('channels') !== false &&
                news &&
                `_ _ ${Emojis.NEWS} ${news} ${Utils.decForInt(news, ['новостной', 'новостных', 'новостных'])}`,
            serverInfoPerms?.get('channels') !== false &&
                stores &&
                `_ _ ${Emojis.STORE} ${stores} ${Utils.decForInt(stores, ['магазин', 'магазина', 'магазинов'])}`,
        ]);
        embed.addField(/*`:open_file_folder: Сводка`*/ '\u200b', summaryList, true);

        const statuses = {
            online: this.membersWithStatus(message, 'online'),
            idle: this.membersWithStatus(message, 'idle'),
            dnd: this.membersWithStatus(message, 'dnd'),
            offline: this.membersWithStatus(message, 'offline'),
        };
        const notCounted =
            message.guild.memberCount - Object.values(statuses).reduce((p: number, c: number) => p + c, 0);
        const statusList = this.list([
            // '\u200b',
            statuses.online && `${Emojis.ONLINE} Онлайн: ${statuses.online}`,
            statuses.idle && `${Emojis.IDLE} Не активны: ${statuses.idle}`,
            statuses.dnd && `${Emojis.DND} Не беспокоить: ${statuses.dnd}`,
            statuses.offline && `${Emojis.OFF} Оффлайн: ${statuses.offline}`,
            notCounted && `${Emojis.EMPTY} Не учтены: ${notCounted}`,
        ]);

        const roles = message.guild.roles.cache.size - 1;
        const emojis = message.guild.emojis.cache.size;
        const otherList = this.list([
            // '\u200b',
            roles && `:flag_white: ${roles} ${Utils.decForInt(roles, ['роль всего', 'роли всего', 'ролей всего'])}`,
            emojis && `:smirk: ${emojis} эмодзи всего`,
        ]);

        const secondaryList = this.list([
            statusList && `${Emojis.USERS} **Статусы**`,
            statusList + '\n',
            otherList && `:bar_chart: **Остальное**`,
            otherList,
        ]);
        if (secondaryList) embed.addField('\u200b', secondaryList, true);
        // if (statusList) embed.addField(`${Emojis.USERS} Статусы`, statusList, true);
        // if (otherList) embed.addField(`:bar_chart: Остальное`, otherList, true);

        return embed;
    }
}
