import { Command, DbInfo } from '../types/Command';
import { Message, MessageEmbed, MessageReaction, ReactionEmoji, User } from 'discord.js';
import Utils from '../Utils';
import { CommandInfo } from '../types/CommandInfo';
import CommandPermissions from '../types/CommandPermissions';
import Emojis from '../types/Emojis';
import asyncTimeout = Utils.asyncTimeout;
import randomInt = Utils.randomInt;
import Palette from '../Palette';

class Tetromino {
    public x: number;
    public y: number;
    public u: number = Math.random();
    public moveQueued?: 'up' | 'down' | 'left' | 'right';

    constructor(
        public readonly id: number,
        private readonly tetris: Tetris,
        public figureStates: number[][][],
        public rotation: number,
        public color: number,
        x?: number,
        y?: number,
    ) {
        this.x =
            typeof x !== 'undefined'
                ? x
                : Math.floor(Tetris.fieldWidth / 2) - Math.floor(this.spawnPoint[2] / 2) + this.spawnPoint[0];
        this.y = typeof y !== 'undefined' ? y : this.spawnPoint[1];
    }

    get spawnPoint(): [number, number, number] {
        return this.figureStates[this.rotation][0] as [number, number, number];
    }

    get figure(): number[][] {
        return this.figureStates[this.rotation].slice(1);
    }

    public queueMove(move: 'up' | 'down' | 'left' | 'right') {
        this.moveQueued = move;
    }

    public rotate(direction: 'clockwise' | 'counterclockwise' = 'clockwise') {
        if (this.tetris.canRotate(this.id, direction)) {
            let rot;
            switch (direction) {
                case 'clockwise':
                    this.rotation = this.rotation === 3 ? 0 : this.rotation + 1;
                    break;
                case 'counterclockwise':
                    this.rotation = this.rotation === 0 ? 3 : this.rotation - 1;
            }
            return true;
        }
        return false;
    }

    public move(direction: 'up' | 'down' | 'left' | 'right'): boolean {
        if (this.tetris.canMove(this.id, direction)) {
            switch (direction) {
                case 'up':
                    this.y--;
                    break;
                case 'down':
                    this.y++;
                    break;
                case 'left':
                    this.x--;
                    break;
                case 'right':
                    this.x++;
            }
            return true;
        }
        return false;
    }

    public fall(): void {
        let status = true;
        while (status) {
            status = this.move('down');
        }
    }
}

class Tetris {
    // spawn x, spawn y, width
    public static readonly figures: number[][][][] = [
        /*
         * OOOO
         * */
        [
            [
                [0, -1, 4],
                [0, 0, 0, 0],
                [1, 1, 1, 1],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            [
                [-2, 0, 1],
                [0, 0, 1, 0],
                [0, 0, 1, 0],
                [0, 0, 1, 0],
                [0, 0, 1, 0],
            ],
            [
                [0, -2, 4],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [1, 1, 1, 1],
                [0, 0, 0, 0],
            ],
            [
                [-1, 0, 1],
                [0, 1, 0, 0],
                [0, 1, 0, 0],
                [0, 1, 0, 0],
                [0, 1, 0, 0],
            ],
        ],
        /*
         * O
         * OOO
         * */
        [
            [
                [0, 0, 3],
                [1, 0, 0],
                [1, 1, 1],
                [0, 0, 0],
            ],
            [
                [-1, 0, 2],
                [0, 1, 1],
                [0, 1, 0],
                [0, 1, 0],
            ],
            [
                [0, -1, 3],
                [0, 0, 0],
                [1, 1, 1],
                [0, 0, 1],
            ],
            [
                [0, 0, 2],
                [0, 1, 0],
                [0, 1, 0],
                [1, 1, 0],
            ],
        ],
        /*
         *   O
         * OOO
         * */
        [
            [
                [0, 0, 3],
                [0, 0, 1],
                [1, 1, 1],
                [0, 0, 0],
            ],
            [
                [-1, 0, 2],
                [0, 1, 0],
                [0, 1, 0],
                [0, 1, 1],
            ],
            [
                [0, -1, 3],
                [0, 0, 0],
                [1, 1, 1],
                [1, 0, 0],
            ],
            [
                [0, 0, 2],
                [1, 1, 0],
                [0, 1, 0],
                [0, 1, 0],
            ],
        ],
        /*
         *  OO
         *  OO
         * */
        [
            [
                [-1, 0, 2],
                [0, 1, 1, 0],
                [0, 1, 1, 0],
                [0, 0, 0, 0],
            ],
            [
                [-1, 0, 2],
                [0, 1, 1, 0],
                [0, 1, 1, 0],
                [0, 0, 0, 0],
            ],
            [
                [-1, 0, 2],
                [0, 1, 1, 0],
                [0, 1, 1, 0],
                [0, 0, 0, 0],
            ],
            [
                [-1, 0, 2],
                [0, 1, 1, 0],
                [0, 1, 1, 0],
                [0, 0, 0, 0],
            ],
        ],
        /*
         *  OO
         * OO
         * */
        [
            [
                [0, 0, 3],
                [0, 1, 1],
                [1, 1, 0],
                [0, 0, 0],
            ],
            [
                [-1, 0, 2],
                [0, 1, 0],
                [0, 1, 1],
                [0, 0, 1],
            ],
            [
                [0, -1, 3],
                [0, 0, 0],
                [0, 1, 1],
                [1, 1, 0],
            ],
            [
                [0, 0, 2],
                [1, 0, 0],
                [1, 1, 0],
                [0, 1, 0],
            ],
        ],
        /*
         *  O
         * OOO
         * */
        [
            [
                [0, 0, 3],
                [0, 1, 0],
                [1, 1, 1],
                [0, 0, 0],
            ],
            [
                [-1, 0, 2],
                [0, 1, 0],
                [0, 1, 1],
                [0, 1, 0],
            ],
            [
                [0, -1, 3],
                [0, 0, 0],
                [1, 1, 1],
                [0, 1, 0],
            ],
            [
                [0, 0, 2],
                [0, 1, 0],
                [1, 1, 0],
                [0, 1, 0],
            ],
        ],
        /*
         * OO
         *  OO
         * */
        [
            [
                [0, 0, 3],
                [1, 1, 0],
                [0, 1, 1],
                [0, 0, 0],
            ],
            [
                [-1, 0, 2],
                [0, 0, 1],
                [0, 1, 1],
                [0, 1, 0],
            ],
            [
                [0, -1, 3],
                [0, 0, 0],
                [1, 1, 0],
                [0, 1, 1],
            ],
            [
                [0, 0, 2],
                [0, 1, 0],
                [1, 1, 0],
                [1, 0, 0],
            ],
        ],
    ];
    public static readonly fieldHeight = 15;
    public static readonly fieldWidth = 7;
    public field: Tetromino[] = [];
    public cache?: number[][];
    private lastRenderedId = 0;
    private counter = 0;
    private removeRows = false;
    public stats = {
        points: 0,
        tetrises: 0,
        tetromino: 0,
        lines: 0,
    };
    public nextFigure = randomInt(0, 6);

    get lastFigure(): Tetromino | undefined {
        return this.field[this.field.length - 1];
    }

    public resetCounter() {
        this.counter = 1;
    }

    tick() {
        this.counter++;
        if (this.removeRows) {
            this.removeRows = false;
            let i = 0;
            this.cache = this.cache
                ?.map((e) => {
                    if (e.includes(8)) {
                        i++;
                        return false;
                    }
                    return e;
                })
                .filter(Boolean) as number[][];
            this.cache?.unshift(...Array(i).fill(Array(Tetris.fieldWidth).fill(0)));
            this.stats.lines += i;
            if (i >= 4) this.stats.tetrises++;
            switch (i) {
                case 0:
                    break;
                case 1:
                    this.stats.points += 100;
                    break;
                case 2:
                    this.stats.points += 300;
                    break;
                case 3:
                    this.stats.points += 700;
                    break;
                default:
                    this.stats.points += 1500;
                    break;
            }
            return this.spawn();
        }
        if (this.field.length === 0 || !this.canMove(this.field.length - 1, 'down')) {
            if (this.field.length !== 0) {
                this.stats.tetromino++;
                const rows = this.getFullRows();
                this.cache = this.render();
                for (const row of rows) {
                    this.cache[row] = Array(Tetris.fieldWidth).fill(8);
                    this.removeRows = true;
                }
                if (!this.removeRows) return this.spawn();
            } else {
                return this.spawn();
            }
        }
        if (this.lastFigure?.moveQueued) {
            this.lastFigure.move(this.lastFigure.moveQueued);
            delete this.lastFigure.moveQueued;
        }
        if (this.counter % 2) this.lastFigure?.move('down');
        return true;
    }

    public getFullRows(): number[] {
        const rendered = this.render();
        return rendered.map((e, i) => (e.every(Boolean) ? i : undefined)).filter((e) => e !== undefined) as number[];
    }

    public canMove(id: number, direction: 'up' | 'down' | 'left' | 'right'): boolean {
        const figure = this.field[id];
        try {
            this.render(
                new Tetromino(
                    figure.id,
                    this,
                    figure.figureStates,
                    figure.rotation,
                    figure.color,
                    direction === 'left' ? figure.x - 1 : direction === 'right' ? figure.x + 1 : figure.x,
                    direction === 'up' ? figure.y - 1 : direction === 'down' ? figure.y + 1 : figure.y,
                ),
            );
        } catch (e) {
            return false;
        }
        return true;
    }

    public canRotate(id: number, direction: 'clockwise' | 'counterclockwise' = 'clockwise'): boolean {
        const figure = this.field[id];
        let rot;
        switch (direction) {
            case 'clockwise':
                rot = figure.rotation === 3 ? 0 : figure.rotation + 1;
                break;
            case 'counterclockwise':
                rot = figure.rotation === 0 ? 3 : figure.rotation - 1;
        }
        try {
            this.render(new Tetromino(figure.id, this, figure.figureStates, rot, figure.color, figure.x, figure.y));
        } catch (e) {
            console.log('cantrotate', e.name);
            return false;
        }
        return true;
    }

    public spawn() {
        const figure = this.nextFigure;
        this.nextFigure = randomInt(0, 6);
        this.field.push(new Tetromino(this.field.length, this, Tetris.figures[figure], randomInt(0, 3), figure + 1));
        try {
            this.render();
        } catch (e) {
            return false;
        }
        return true;
    }

    public render(figure?: Tetromino): number[][] {
        if (!figure) {
            figure = this.lastFigure;
            figure = figure as Tetromino;

            if (typeof this.lastFigure === 'undefined')
                return new Array(Tetris.fieldHeight)
                    .fill(undefined)
                    .map(() => new Array(Tetris.fieldWidth).fill(undefined).map(() => 0));
            // if (this.lastRenderedId !== figure.id) {
            // console.log(this.lastRenderedId, figure.id);
            // this.cache = this.render(this.field[this.lastRenderedId]) as number[][];
            // }
            this.lastRenderedId = figure.id;
        }

        const field = this.cache
            ? JSON.parse(JSON.stringify(this.cache))
            : new Array(Tetris.fieldHeight)
                  .fill(undefined)
                  .map(() => new Array(Tetris.fieldWidth).fill(undefined).map(() => 0));

        if (!this.removeRows) {
            const figureArray = figure.figure;
            for (const rowIndex in figureArray) {
                const row = figureArray[rowIndex];
                for (const blockIndex in row) {
                    const block = row[blockIndex];
                    if (block === 0) continue;
                    if (!(figure.y + +rowIndex in field) || !(figure.x + +blockIndex in field[figure.y + +rowIndex]))
                        throw Error('wall');
                    if (field[figure.y + +rowIndex][figure.x + +blockIndex]) throw Error('collision');
                    field[figure.y + +rowIndex][figure.x + +blockIndex] = block ? figure.color : 0;
                }
            }
        }
        return field;
    }
}

const commandArgs = {};
export default class TetrisCommand implements Command {
    public info: CommandInfo = {
        name: 'tetris',
        trigger: ['tetris'],
        shortDescription: 'test short',
        description: 'test long',
        args: commandArgs,
        showInHelp: false,
        disableWrite: false,
    };

    private getEmoji(num: number) {
        switch (num) {
            case 0:
                return '▪️';
            case 1:
                return '🟥';
            case 2:
                return '🟫';
            case 3:
                return '🟧';
            case 4:
                return '🟨';
            case 5:
                return '🟩';
            case 6:
                return '🟦';
            case 7:
                return '🟪';
            case 8:
                return '⬜';
        }
    }

    static readonly paintings = [
        [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 1],
            [0, 1, 0, 0, 0, 1, 0],
            [0, 0, 1, 0, 1, 0, 0],
            [0, 0, 0, 1, 0, 0, 0],
            [0, 0, 1, 0, 1, 0, 0],
            [0, 1, 0, 0, 0, 1, 0],
            [1, 0, 0, 0, 0, 0, 1],
        ],
        [
            [],
            [],
            [],
            [8, 8, 8, 8],
            [8, 4, 4, 8],
            [8, 4, 4, 8, 8, 8, 8],
            [8, 4, 4, 3, 3, 3, 8],
            [8, 4, 4, 3, 4, 4, 8],
            [8, 4, 4, 3, 3, 3, 8],
            [8, 4, 4, 3, 4, 4, 8],
            [8, 4, 4, 3, 3, 3, 8],
            [8, 8, 8, 8, 8, 8, 8],
        ],
    ];

    private lastState: number[][] = [];

    format(tetris: Tetris) {
        this.lastState = tetris.render();
        const embed = new MessageEmbed().setColor(Palette.EMBED_COLOR);
        embed
            .addField('\u200b', this.lastState.map((e) => e.map(this.getEmoji).join('')).join('\n') || '\u200b', true)
            .addField('\u200b', '\u200b', true);
        embed.addField(
            '\u200b',
            '**Статистика**\n\n' +
                '```' +
                `Очков: ${tetris.stats?.points || 0}\n` +
                `Блоков: ${tetris.stats?.tetromino || 0}\n` +
                `Линий: ${tetris.stats?.lines || 0}\n` +
                `Тетрисов: ${tetris.stats?.tetrises || 0}\n` +
                '```' +
                (tetris.nextFigure !== undefined
                    ? '\n\n**След. фигура**\n\n' +
                      (Tetris.figures[tetris.nextFigure][0]
                          .slice(1)
                          .map((e) => '\u200b  ' + e.map((e) => this.getEmoji(e ? tetris.nextFigure + 1 : 0)).join(''))
                          .join('\n') || '\u200b')
                    : ''),
            true,
        );
        return {
            // content: tetris.stats
            //     ? `Очков: ${tetris.stats.points} | Тетромино: ${tetris.stats.tetromino} | Линий: ${tetris.stats.lines} | Тетрисов: ${tetris.stats.tetrises}`
            //     : '',
            content: '',
            embed,
        };
    }

    private serversPlaying: { [key: string]: string } = {};

    async run(message: Message, args: any, command: string, db: DbInfo): Promise<any> {
        if (!message.guild) return;
        if (this.serversPlaying[message.guild.id]) {
            return `На этом сервере уже ведётся игра. Игрок: ${
                message.guild?.members.resolve(this.serversPlaying[message.guild?.id || ''])?.user.tag
            }`;
        }
        this.serversPlaying[message.guild.id] = message.author.id;
        try {
            const msg = await message.channel.send('Загрузка...');
            const tetris = new Tetris();
            let gaveUp = false;

            const collector = msg.createReactionCollector(
                (reaction: MessageReaction, user: User) =>
                    user.id === message.author.id && ['⬅️', '➡️', '⬇️', '↩️', '↪️', '❌'].includes(reaction.emoji.name),
            );
            collector.on('collect', async (reaction, user) => {
                switch (reaction.emoji.name) {
                    case '⬅️':
                        if (tetris.lastFigure?.queueMove('left')) await msg.edit(this.format(tetris));
                        break;
                    case '➡️':
                        if (tetris.lastFigure?.queueMove('right')) await msg.edit(this.format(tetris));
                        break;
                    case '⬇️':
                        tetris.lastFigure?.fall();
                        tetris.resetCounter();
                        await msg.edit(this.format(tetris));
                        break;
                    case '↩️':
                        if (tetris.lastFigure?.rotate()) await msg.edit(this.format(tetris));
                        break;
                    case '↪️':
                        if (tetris.lastFigure?.rotate('counterclockwise')) await msg.edit(this.format(tetris));
                        break;
                    case '❌':
                        gaveUp = true;
                }
                await reaction.users.remove(user);
            });

            await msg.react('⬅️');
            await msg.react('➡️');
            await msg.react('↩️');
            await msg.react('↪️');
            await msg.react('⬇️');
            await msg.react('❌');

            await msg.edit(this.format(tetris));
            while (!gaveUp && tetris.tick()) {
                await asyncTimeout(1300);
                await msg.edit(this.format(tetris));
            }

            if (gaveUp || tetris.stats.points >= 1000) {
                const painting = TetrisCommand.paintings[gaveUp ? 0 : 1];
                for (const rowIndex in painting) {
                    const row = painting[rowIndex];
                    for (const colIndex in row) {
                        const col = row[colIndex];
                        if (col) this.lastState[rowIndex][colIndex] = col;
                    }
                }
                await msg.edit(
                    this.format({
                        render: () => {
                            return this.lastState;
                        },
                        stats: tetris.stats,
                    } as Tetris),
                );
            }

            delete this.serversPlaying[message.guild.id];
            await message.channel.send(
                new MessageEmbed()
                    .setTitle('Game Over')
                    .addField(
                        'Статистика:',
                        `- Очков набрано: **${tetris?.stats?.points || 0}**\n` +
                            `- Тетромино поставлено: **${tetris?.stats?.tetromino || 0}**\n` +
                            `- Линий сделано: **${tetris?.stats?.lines || 0}**\n` +
                            `- Тетрисов (4 линии) сделано: **${tetris?.stats?.tetrises || 0}**`,
                    )
                    .setColor(
                        tetris.stats.points >= 1000 ? '#FDCB58' : tetris.stats.points === 0 ? '#DD2E44' : '#55ACEE',
                    ),
            );
            if (tetris.stats.points > (db.user.gameScores.get('tetris') || 0)) {
                db.user.gameScores.set('tetris', tetris.stats.points);
                await db.user.save();
            }
        } catch (e) {
            if (this.serversPlaying[message.guild.id]) delete this.serversPlaying[message.guild.id];
            throw e;
        }
    }
}
