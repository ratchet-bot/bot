import { Arguments, Command, DbInfo } from '../types/Command';
import { GuildChannel, GuildMember, Message, MessageEmbed } from 'discord.js';
import Icons from '../Icons';
import color from '../Palette';
import { CommandInfo } from '../types/CommandInfo';
import CommandService from '../services/CommandService';
import MessageService from '../services/MessageService';
import Utils from '../Utils';
import { CommandStatus } from '../types/CommandStatus';

const commandArgs = {
    element: {
        type: String,
        required: false,
        title: 'категория или команда',
    },
};
export default class HelpCommand implements Command {
    public info: CommandInfo = {
        name: 'help',
        trigger: [/^h[ae]lp/i, /^х[еэ]лп$/i, /^пом[оа](щ|ги)$/i],
        aliases: ['help', 'помощь'],
        shortDescription: 'Помощь',
        description: 'С помощью этой команды вы можете получить список доступных вам команд.',
        listInHelp: false,
        args: commandArgs,
        immutable: true,
    };

    list(elements: (string | false | null | undefined | 0)[], dot = ''): string | false {
        return elements.filter(Boolean).length
            ? elements
                  .filter(Boolean)
                  .map((e) => (e === '\u200b' ? e : dot + e))
                  .join('\n')
            : false;
    }

    async run(message: Message, args: Arguments<typeof commandArgs>, command: string, db: DbInfo): Promise<any> {
        const prefix = db.guild.prefix;
        const commandService = await CommandService.build();
        const messageService = await MessageService.build();
        const commands = commandService.registeredCommands.filter(
            (c: Command) =>
                c.info.showInHelp !== false &&
                message.member &&
                message.guild &&
                messageService.checkPermissions(
                    c.info.permission || [],
                    c.info.botPermission || [],
                    message.member,
                    message.channel as GuildChannel,
                    false,
                    false,
                ),
        );
        const categories = [...new Set(commands.map((c: Command) => c.info.regExpCategory))];

        if (args.element) {
            const category = categories.find(
                (c?: [string, RegExp[]]) =>
                    c?.[0]?.toLowerCase() === args.element.toLowerCase() ||
                    c?.[1]?.some((r: RegExp) => args.element.match(r)),
            );
            let command = commandService.resolveCommand(args.element);
            if (command?.info.showInHelp === false) command = undefined;

            if (category || args.element.match(/^al+|вс[её]$/i)) {
                let helpCommands;
                if (category)
                    helpCommands = commands.filter((c: Command) => c.info.regExpCategory?.[0] === category[0]);
                else helpCommands = commands;

                const longest = Math.max(...helpCommands.map((c) => c.info.name.length));

                return new MessageEmbed()
                    .setColor(color.GREEN)
                    .setAuthor('Помощь', Icons.BOT_GREEN)
                    .addField(
                        `${category?.[0] || 'Команды'}:`,
                        `\`\`\`css\n${helpCommands
                            .filter(
                                (c: Command) =>
                                    c.info.listInHelp !== false &&
                                    ![CommandStatus.DISABLED, CommandStatus.DISABLED_HERE].includes(
                                        commandService.getStatus(c, message, db).status,
                                    ),
                            )
                            .map(
                                (c: Command) =>
                                    ' ' +
                                    c.info.name +
                                    ' ' +
                                    (commandService.getStatus(c, message, db).status === CommandStatus.NEEDS_ACTION
                                        ? '!'
                                        : ' ') +
                                    ' '.repeat(longest - c.info.name.length) +
                                    '— ' +
                                    c.info.shortDescription +
                                    ' ',
                            )
                            .join('\n')}\`\`\``,
                    )
                    .setFooter(`Напишите „${prefix}help [название команды]“ для большей детализации.`);
            } else if (command) {
                const status = await commandService.getStatus(command, message, db);
                const embed = new MessageEmbed()
                    .setColor(color.GREEN)
                    .setAuthor('Помощь по команде „' + command.info.name + '“', Icons.BOT_GREEN)
                    .addField(
                        'Информация:',
                        this.list(
                            [
                                `\u200b`,
                                `Название: \`${command.info.name}\``,
                                command.info.aliases &&
                                    command.info.aliases.length &&
                                    `Алиасы: \`${command.info.aliases.join('`, `')}\``,
                                command.info.regExpCategory && `Категория: \`${command.info.regExpCategory[0]}\``,
                                command.info.cooldown &&
                                    `Категория: \`${Utils.formatDuration(command.info.cooldown)}\``,
                            ],
                            '',
                        ),
                        true,
                    )
                    .addField(
                        'Доступ:',
                        '```diff\n' +
                            this.list([
                                status.status === CommandStatus.DISABLED ? '- Команда выключена' : '+ Команда включена',
                                [CommandStatus.DISABLED, CommandStatus.DISABLED_HERE].includes(status.status)
                                    ? '- Команда не доступна тут'
                                    : '+ Команда доступна тут',
                                messageService.checkPermissions(
                                    command.info.permission || [],
                                    command.info.botPermission || [],
                                    message.member as GuildMember,
                                    message.channel as GuildChannel,
                                    false,
                                    false,
                                )
                                    ? '+ У вас есть права'
                                    : '- У вас нет прав',
                            ]) +
                            '```',
                        true,
                    )
                    .addField(
                        'Использование:',
                        `\`\`\`css\n${command.info.name} ${
                            command.info.usage ||
                            Object.keys(command.info.args || {})
                                .map(
                                    (e) =>
                                        '[' +
                                        (command?.info.args?.[e]?.title || e) +
                                        (!command?.info.args?.[e]?.required ? '?' : '') +
                                        ']',
                                )
                                .join(' ')
                        }\`\`\``,
                    )
                    .addField('Описание:', command.info.description)
                    .setFooter('Аргументы, помеченные символом „?“ в конце, являются необязательными.');

                const names = {
                    [CommandStatus.DISABLED]: 'Причина отключения:',
                    [CommandStatus.DISABLED_HERE]: 'Причина отключения:',
                    [CommandStatus.NEEDS_ACTION]: 'Команда требует дополнительных действий:',
                    [CommandStatus.ENABLED]: 'Предупреждение:',
                };
                if (status.info) embed.addField(names[status.status], '```fix\n' + status.info + '```');

                return embed;
            }
        }

        return new MessageEmbed()
            .setColor(color.GREEN)
            .setAuthor('Помощь', Icons.BOT_GREEN)
            .setDescription(
                `Напишите \`${prefix}help [категория]\`, \nили \`${prefix}help all\` для получения списка команд.`,
            )
            .addField(
                `:file_folder: Категории:`,
                `\`\`\`diff\n! ${categories.map((c?: [string, RegExp[]]) => c?.[0]).join('\n! ')}\`\`\``,
            );
    }
}
