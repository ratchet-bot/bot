import { Arguments, Command, DbInfo } from '../types/Command';
import { GuildChannel, Message, MessageEmbed, MessageMentions, Role } from 'discord.js';
import { CommandInfo } from '../types/CommandInfo';
import Utils from '../Utils';
import Bot from '../Bot';
import CommandPermissions from '../types/CommandPermissions';
import CommandService from '../services/CommandService';
import { DBGuildCommandSettings } from '../models/guild/DBGuildCommandSettings';
import waitForMessage = Utils.waitForMessage;

type Target =
    | Paths[]
    | {
          param: string;
          type:
              | StringConstructor
              | BooleanConstructor
              | NumberConstructor
              | typeof GuildChannel
              | typeof Role
              | [typeof GuildChannel];
          isEnabled?: (db: DbInfo, command?: string) => boolean;
      };

type Paths = {
    name: string;
    trigger: RegExp;
    target: Target;
};

const commandArgs = {
    path: {
        type: String,
        required: false,
        title: 'путь к опции',
    },
    value: {
        type: String,
        required: false,
        title: 'значение',
    },
};
export default class SettingsCommand implements Command {
    public info: CommandInfo = {
        name: 'settings',
        trigger: [/^set(tings?)$/i, /^params?$/i, /^на_?стр(ойк[ие])?$/i],
        aliases: ['settings', 'настройки'],
        shortDescription: 'Пригласить бота',
        description: 'С помощью этой команды Вы можете настроить разные аспекты данного бота.',
        permission: CommandPermissions.OWNER,
        args: commandArgs,
        oneMessage: true,
    };

    public separator = /[\/\\.>\-:]+/;

    static paths: Paths[] = [
        {
            name: 'Модули',
            trigger: /модул[еиь]?|module?/i,
            target: [
                {
                    name: 'Мут',
                    trigger: /mute?|мут/i,
                    target: [
                        {
                            name: 'Включен',
                            trigger: /^вы?ключ[её]н[оа]?|(dis|en)abled?$/i,
                            target: {
                                param: 'guild.settings.mute.enabled',
                                type: Boolean,
                            },
                        },
                        {
                            name: 'Логгировать',
                            trigger: /^лог+(ировать)?|log$/i,
                            target: {
                                param: 'guild.settings.mute.log',
                                type: Boolean,
                            },
                        },
                        // {
                        //     name: 'Канал логов',
                        //     trigger: /^к?анал[ _]?(лог(ов)?)?|log[ _]?chan(nel)?$/i,
                        //     target: {
                        //         param: 'guild.settings.mute.logChannel',
                        //         type: GuildChannel,
                        //         isEnabled: (db) => db.guild.settings.mute.log || false,
                        //     },
                        // },
                    ],
                },
                {
                    name: 'Уровни',
                    trigger: /^levels?|уров(ень?|ни)$/i,
                    target: [
                        {
                            name: 'Включен',
                            trigger: /^вы?ключ[её]н[оа]?|(dis|en)abled?$/i,
                            target: {
                                param: 'guild.settings.levels.enabled',
                                type: Boolean,
                            },
                        },
                    ],
                },
            ],
        },
        {
            name: 'Префикс',
            trigger: /^префикс|prefi(x|ks)$/i,
            target: {
                param: 'guild.prefix',
                type: String,
            },
        },
        {
            name: 'Команды',
            trigger: /^ком+анд[аы]?|com+ands?$/i,
            target: [
                {
                    name: 'Включен',
                    trigger: /^включ[её]н[оа]?|enabled?$/i,
                    target: {
                        param: 'guild.commandSettings.%s.enabled',
                        type: Boolean,
                    },
                },
                // {
                //     name: 'Разрешенные каналы',
                //     trigger: /^allow(ed)?[ _]?(chan+(els?)?)?|разреш[её]н+(ые|о|ый)?[ _]?к?анал+ы?$/i,
                //     target: {
                //         param: 'guild.commandSettings.%s.allowedChannels',
                //         type: [GuildChannel],
                //         isEnabled: (db: DbInfo, command?: string) => {
                //             console.log('b', command);
                //             return db.guild.commandSettings.get(command || '_')?.enabled || false;
                //         },
                //     },
                // },
                // {
                //     name: 'Запрещённые каналы',
                //     trigger: /^block(ed)?[ _]?(chan+(els?)?)?|запрещ[её]н+(ые|о|ый)?[ _]?к?анал+ы?$/i,
                //     target: {
                //         param: 'guild.commandSettings.%s.deniedChannels',
                //         type: [GuildChannel],
                //         isEnabled: (db: DbInfo, command?: string) =>
                //             db.guild.commandSettings.get(command || '_')?.enabled || false,
                //     },
                // },
            ],
        },
    ];

    private formatValue(val: any): string {
        switch (val) {
            case true:
                return 'да';
            case false:
                return 'нет';
            case undefined:
            case null:
                return 'не установлено';
            default:
                if (Array.isArray(val)) return val.join(', ');
                return String(val);
        }
    }

    private async getPath(currentPath: string[]): Promise<Paths | false> {
        const commandService = await CommandService.build();
        let current: Paths = {
            name: 'Главная',
            trigger: /^$/,
            target: SettingsCommand.paths,
        };
        if (!currentPath.length) return current;
        for (const pathIndex in currentPath) {
            const path = currentPath[pathIndex];
            let element;
            if (current.name === 'Команды') {
                const command = commandService.resolveCommand(path);
                if (!command || command.info.immutable) return false;
                const trigger = Array.isArray(command?.info.trigger) ? command?.info.trigger : [command?.info.trigger];
                element = {
                    name: command?.info.name || '',
                    trigger: (trigger
                        ?.flat()
                        .map((e) =>
                            typeof e === 'string' ? new RegExp('\\' + e.split('').join('\\')) : e,
                        ) as RegExp[]).reduce(
                        (prev, curr) => new RegExp(prev.source + '|' + curr.source),
                        new RegExp(''),
                    ),
                    target: (current.target as Paths[]).map((e) => ({
                        ...e,
                        target: {
                            param: (e.target as any).param.replace(/\%s/g, command.info.name),
                            type: (e.target as any).type,
                        },
                    })),
                };
            } else element = (current.target as Paths[]).find((e) => path.match(e.trigger));
            if (!element) return false;
            if (+pathIndex === currentPath.length - 1) return element;
            if (!Array.isArray(element.target)) return false;
            current = element;
        }
        return false;
    }

    db(db: DbInfo, arr: string[] | string, value?: any) {
        if (typeof arr === 'string') arr = arr.split(this.separator);
        const group = arr.shift();
        arr = arr.join('.');

        if (typeof value !== 'undefined') {
            if (arr.includes('commandSettings')) {
                const data = arr.split('commandSettings.')?.[1].split('.');
                if (!db.guild.commandSettings.has(data[0])) {
                    db.guild.commandSettings.set(data[0], { enabled: true });
                }
                db.guild.commandSettings.set(data[0], {
                    ...((db.guild.commandSettings.get(data[0]) as any)?._doc as DBGuildCommandSettings),
                    [data[1]]: value,
                });
                console.log({
                    ...(db.guild.commandSettings.get(data[0]) as DBGuildCommandSettings),
                    [data[1]]: value,
                });
                db.guild.markModified('commandSettings');
            }
            db[group as 'member'].set(arr, value);
            db[group as 'member'].markModified(arr);
        }
        return db[group as 'member'].get(arr);
    }

    async *run(
        message: Message,
        args: Arguments<typeof commandArgs>,
        command: string,
        db: DbInfo,
        send: any,
    ): AsyncGenerator<any> {
        ((db: any) => db)(db);
        const currentPath: string[] = [];
        let pathFromArgs = !!args.path;
        let valueFromArgs = !!args.value;
        let wasOneTime = !!args.value;
        let save = true;
        Bot.inInteraction.push(message.author.id);
        mainLoop: for (;;) {
            const embed = new MessageEmbed().setTitle(['Главная', ...currentPath].join(' » '));
            const oldPath = await this.getPath(currentPath);
            if (oldPath && Array.isArray(oldPath.target)) {
                if (currentPath[currentPath.length - 1] === 'Команды') {
                    embed.setDescription('Введите название команды блядь))');
                } else
                    embed.setDescription(
                        oldPath.target
                            .filter((t) => {
                                console.log('a', currentPath.join('.'));
                                return 'isEnabled' in t.target
                                    ? t.target?.isEnabled?.(db, currentPath[currentPath.length - 1])
                                    : true;
                            })
                            .map(
                                (t) =>
                                    t.name +
                                    ('param' in t.target ? ': ' + this.formatValue(this.db(db, t.target.param)) : ''),
                            )
                            .join('\n'),
                    );
            }

            const pathArray = pathFromArgs
                ? args.path.split(this.separator)
                : (await waitForMessage(message, embed, 60000, true, send))?.split(this.separator);
            pathFromArgs = false;

            if (!pathArray) break;
            if (pathArray?.[0] === 'save') break;
            if (pathArray?.[0] === 'cancel') {
                save = false;
                break;
            }
            if (pathArray?.[0] === 'back') currentPath.pop();

            let targetPath: Paths = {
                name: 'Главная',
                trigger: /^$/,
                target: SettingsCommand.paths,
            };
            for (const currentPathString of pathArray) {
                const currentTargetPath = await this.getPath([...currentPath, currentPathString]);
                if (!currentTargetPath) {
                    valueFromArgs = false;
                    wasOneTime = false;
                    continue mainLoop;
                }
                if (Array.isArray(currentTargetPath.target)) currentPath.push(currentTargetPath.name);
                targetPath = currentTargetPath;
            }

            console.log(pathArray);
            if (!targetPath || targetPath.name === 'Главная') continue;
            if (Array.isArray(targetPath.target)) {
                continue;
            }

            const param = this.db(db, targetPath.target.param); // todo f
            const paramValue = valueFromArgs
                ? args.value
                : await waitForMessage(
                      message,
                      `!!!Установите значение для параметра \`${
                          targetPath.name
                      }\`. Текущее значение: \`${this.formatValue(param)}\``,
                      60000,
                      true,
                      send,
                  );

            console.log(targetPath.target.param);

            let sentMessage;
            switch (targetPath.target.type) {
                case Number:
                    if (isNaN(Number(paramValue))) {
                        sentMessage = yield '!!!Неправильное значение';
                        continue;
                    }
                    this.db(db, targetPath.target.param, Number(paramValue));
                    break;
                case Boolean:
                    if (
                        !['false', 'n', 'no', 'нет', 'true', 'y', 'yes', 'да'].includes(paramValue?.toLowerCase() || '')
                    ) {
                        sentMessage = yield '!!!Неправильное значение';
                        continue;
                    }
                    this.db(
                        db,
                        targetPath.target.param,
                        ['да', 'true', 'y', 'yes'].includes(paramValue?.toLowerCase() || ''),
                    );
                    break;
                case String:
                    this.db(db, targetPath.target.param, String(paramValue));
                case [GuildChannel]:
                    const arr = [];
                    for (const el of String(paramValue).split(',')) {
                        const match = el.trim().match(MessageMentions.CHANNELS_PATTERN);
                        if (!match) {
                            sentMessage = yield '!!!Неправильное значение';
                            continue;
                        }
                        arr.push(match);
                    }
                    console.log(arr);
                    this.db(db, targetPath.target.param, arr);
            }

            if (sentMessage) (sentMessage as Message).delete({ timeout: 3000 }).catch(() => undefined);

            if (valueFromArgs) {
                valueFromArgs = false;
                break;
            }
        }
        Bot.inInteraction = Bot.inInteraction.filter((e) => e !== message.author.id);
        if (save) {
            await db.guild.save();
            await db.member.save();
            await db.user.save();
        }
        return !save && !wasOneTime ? 'Настройки были отменены' : 'Настройки были сохранены';
    }
}
