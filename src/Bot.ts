import { Client, Message, MessageEmbed } from 'discord.js';
import MessageService from './services/MessageService';
import CommandService from './services/CommandService';
import { MessageError } from './errors/MessageError';
import { Command } from './types/Command';
import DatabaseService from './services/DatabaseService';
import ActionService from './services/ActionService';
import Utils from './Utils';
import Icons from './Icons';
import randomInt = Utils.randomInt;
import calculateLevel = Utils.calculateLevel;

export default class Bot {
    public static categories: { [key: string]: [string, RegExp[]] } = {
        reactions: ['Эмоции', [/^emot(ions|es)?$/i, /^react(ions)?$/i, /^[еэ]моц[иы]+$/i, /^р[еэ]акц[иы]+$/i]],
        moderation: ['Модерация', [/^mod(er(ation)?)?$/i, /^мод([еэ]р(ац[ыи](я|йа)?)?)?$/i]],
        _: ['Другие', [/^other$/, /^друг([ыи]й?е|ой)$/i]],
    };
    public static owners: string[] = ['340938899285344258', '421030089732653057'];
    public static defaultPrefix = process.env.NODE_ENV === 'development' ? '>' : '.';
    private static instance?: Bot;
    public client!: Client;
    private messageService!: MessageService;
    private commandService!: CommandService;
    private dbService!: DatabaseService;
    private actionService!: ActionService;
    private xpLog: { [key: string]: number } = {};
    public static inInteraction: string[] = [];

    private constructor() {
        /**/
    }

    public static build(): Bot {
        return Bot.instance || (Bot.instance = new Bot());
    }

    public async bootstrap(): Promise<void> {
        this.client = new Client();
        this.dbService = await DatabaseService.build();
        this.messageService = MessageService.build();
        this.actionService = ActionService.build();
        this.commandService = await CommandService.build(this.client);

        // console.log(await this.dbService.getDbGuild('abcdef'));

        await this.client.login(process.env.TOKEN);
        console.log('Discord ready!');

        await this.commandService.registerCommands(this.client);
        console.log('Commands registered');

        this.commandService.subscribeEvents(this.client);
        console.log('Events subscribed');

        this.client.on('message', this.onMessage.bind(this));
        console.log('Bot ready');
        // client.on('message', async (message) => {
        //     if (message.author.bot) return;
        //     try {
        //         const info = await messageService.parseMessage(message, '.');
        //         if (!info || !Object.keys(info).length) return;
        //
        //         message.channel.send('```json\n' + JSON.stringify(info, null, 2) + '```');
        //     } catch (e) {
        //         if (e instanceof MessageError) {
        //             return message.channel.send(e.toMessage());
        //         }
        //         message.channel.send('ERROR!\n```json\n' + JSON.stringify(e, null, 2) + '```');
        //     }
        // });
    }

    private async onMessage(message: Message): Promise<void> {
        if (message.author.bot) return;
        if (!message.guild || !message.member) return;
        if (Bot.inInteraction.includes(message.author.id)) return;
        // if (process.env.NODE_ENV && !Bot.owners.includes(message.author.id)) return;

        const guild = await this.dbService.getDbGuild(message.guild);
        if (guild.blacklisted) return;

        if (
            message.mentions.users.has(message.client.user?.id || '') &&
            message.content.match(new RegExp(`^<@!?${message.client.user?.id}>`))
        )
            return void (await message.reply(`Мой префикс на этом сервере: \`${guild.prefix}\``));

        const user = await this.dbService.getDbUser(message.author);
        if (user.blacklisted) return;

        const member = await this.dbService.getDbMember(message.member);

        // await member.message(guild); // swear stat

        try {
            if (!(member.id in this.xpLog) || this.xpLog[member.id] + 5000 < Date.now()) {
                const xp = randomInt(15, 25);
                // await this.dbService.setDbMemberXP(member.id, member.rank + xp);
                // await this.dbService.setUserXP(user.id, user.rank + xp);
                this.xpLog[member.id] = Date.now();
                const { level } = calculateLevel(member.xp + xp);
                if (calculateLevel(member.xp).level !== level)
                    await message.channel.send(
                        new MessageEmbed()
                            .setAuthor(`Пользователь ${message.author.tag} получает ${level} уровень`, Icons.TWOUSER)
                            .setColor(`#585f63`),
                    );

                await member.addXP(xp);
                await user.addXP(xp);
            }
        } catch (e) {
            /**/
        }

        let info: { command: string; args: { [p: string]: any }; commandObject: Command } | undefined;
        try {
            info = await this.messageService.parseMessage(message, guild.prefix, { guild, user, member });
            if (!info || !Object.keys(info).length) return; // todo throw no command found error

            const oneMessage = info.commandObject.info.oneMessage;
            let baseMessage: Message | undefined;

            if (oneMessage) {
                baseMessage = await message.channel.send('\u200b');
            } else if (info.commandObject.info.disableWrite) message.channel.startTyping().catch(() => null);

            await this.messageService.resolveCommandResponse(
                message,
                await info.commandObject.run(
                    message,
                    info.args,
                    info.command,
                    { guild, user, member },
                    (content: any) => this.messageService.resolveResponse(message, content, baseMessage),
                ),
                baseMessage,
            );

            if (!oneMessage && !info.commandObject.info.disableWrite) await message.channel.stopTyping();
        } catch (e) {
            Bot.inInteraction = Bot.inInteraction.filter((e) => e !== message.author.id);
            console.error(e);
            await message.channel.stopTyping(true);
            if (typeof e === 'object' && 'toMessage' in e) await message.channel.send(e.toMessage());
            else await message.channel.send(new MessageError('Произошла внутренняя ошибка').toMessage());
        }
    }
}
