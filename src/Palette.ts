enum Palette {
    RED = '#f36c6c',
    GRAY = '#585f63',
    GREEN = '#69c49a',
    YELLOW = '#fbb848',
    GREEN_BH = '#43b581',
    BLUE_DEV = '#7289da',
    YELLOW_AMB = '#f8a532',
    EMBED_COLOR = '#2f3036',
    PINK_DONATOR = '#ff73fa',
    BLUE_PARTNER = '#3f86ec',
    REMOVE = '#f44949',
    ADD = '#43b581',
}

export default Palette;
