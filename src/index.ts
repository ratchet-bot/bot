import Bot from './Bot';

const bot = Bot.build();
bot.bootstrap().catch(console.error);
